import numpy as np
import os

import Default

if Default.DEBUG_MODE:
    from IPython import embed

FILES_PATH_DICT = {"Mesh":"Mesh/NodeCoords.txt",
                   "ElementNodes":"Mesh/ElemNodes.txt",
                   "Bathymetry":"InitialConditions/Bathymetry.txt",
                   "engine":"ANSWER/engine.txt",
                   "ArrivalTimes":"ANSWER/ArrivalTimes.txt",
                   "MaxWaterHeights":"ANSWER/MaxWaterHeights.txt",
                   "TimeSeries":"ANSWER/TimeSeriesValues.txt",
                   "SpatialProfiles":"ANSWER/SpatialProfiles.txt",
                   }

LOAD_FILES_DICT = {"ic":[],
                    "maps":["Mesh", "Bathymetry", "ElementNodes", "ArrivalTimes", "MaxWaterHeights"],
                    "ts":[],
                    "sp":[],
                    "sim":[],
                    }


################################################################################
# Check if format is valid
################################################################################
def has_valid_format(fpath, data_source):
    """
    Check if provided path has the simplified format.
    """
    if data_source=="QUESTION":
    # Boolean for question structure
        question_folders = ["Mesh", "InitialConditions", "Output"]
        in_question_format = all( os.path.exists(os.path.join(fpath,folder)) for folder in question_folders)
        return in_question_format
    elif data_source=="ANSWER":
        answer_folders = ["ANSWER", "Mesh", "InitialConditions", "Output"]
        in_answer_format = all( os.path.exists(os.path.join(fpath,folder)) for folder in question_folders)
        return in_answer_format
    else:
        return False

################################################################################
# LOAD DATA IN SIMPLIFIED FORMAT
################################################################################
def load(from_path, load_key):
    """
    Returns the qas dictionary with the data available in the provided folder.
    """
    print("Reading files in QAS format.")
    qas_dict = {}

    for key in LOAD_FILES_DICT[load_key]:
        relative_file_path = FILES_PATH_DICT[key]
        file_path = os.path.join(from_path, relative_file_path)
        if os.path.exists(file_path):
            print "\tLoading {0}".format(key)
            qas_dict[key] = np.loadtxt(file_path)

    # Fixing MaxWaterHeight
    if "MaxWaterHeights" in qas_dict.keys():
        qas_dict["InitialDryCells"] = qas_dict["MaxWaterHeights"][0,:].astype(bool)
        qas_dict["MaxWaterHeights"] = qas_dict["MaxWaterHeights"][1,:]

    # Fixing some integer arrays
    int_arrays = ["GhostCells", "ElementNodes"]
    for key in int_arrays:
        if key in qas_dict.keys():
            qas_dict[key] = qas_dict[key].astype(int)

    return qas_dict
