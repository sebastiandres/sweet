import sys
import os

import SpatialProfilePlotlib
import TimeSeriesPlotlib
import DebugMesh
import MeshProperties
import GMSH4SBF
import GMSH4maps
import Default
import Common
import GMSH
import AVI

if Default.DEBUG_MODE:
    from IPython import embed

################################################################################
# Plot the mesh values or the query point values
################################################################################
def plot(full_path, options):
    # Check if provided folder exists
    folder_exists = Common.check_dir(full_path)
    if not folder_exists:
        print("Provided folder does not exists")
        return sys.exit(-1)
    # Generate the folder where sweet will save its data
    folder = "SWEET"
    # Generate the local paths
    sweet_local_path = os.path.join(options["dest_folder"], folder)
    sweet_full_path = os.path.abspath(os.path.join(full_path, folder))
    mesh_full_path = os.path.abspath(os.path.join(full_path, "Mesh"))
    m2d_full_path = os.path.join(mesh_full_path, "Optional")
    show_in_degrees = options["degrees"]

    Common.mkdir(sweet_full_path)

    if options["sbf"]:
        GMSH4SBF.export_to_gmsh(full_path, show_in_degrees)
        return True

    if options["maps"]:
        GMSH4maps.export_to_gmsh(full_path)
        return True

    if options["statistics"]:
        DebugMesh.create(full_path, sweet_full_path, sweet_local_path,
                         show_in_degrees=show_in_degrees,
                         zoom_window=options["zoom"])
        return True

    if options["simulation"]:
        sweet_full_path = os.path.join(sweet_full_path, "simulation")
        sweet_local_path = os.path.join(sweet_local_path, "simulation")
        Common.mkdir(sweet_full_path)
        GMSH.create(full_path, sweet_full_path, sweet_local_path, options["scale"], "answer",
                    frame=options["frame"],
                    show_in_degrees=show_in_degrees,
                    zoom_window=options["zoom"])
        return True

    if options["initial_conditions"]:
        GMSH.create(full_path, sweet_full_path, sweet_local_path, options["scale"], "question",
                    show_in_degrees=show_in_degrees,
                    zoom_window=options["zoom"])
        return True

    if options["spatial_profiles"]:
        imgs_local_path = os.path.join(sweet_local_path, "spatial_profiles")
        imgs_full_path = os.path.abspath(imgs_local_path)
        Common.mkdir(imgs_full_path)
        SpatialProfilePlotlib.create(full_path, imgs_full_path, imgs_local_path,
                                     show_in_degrees=show_in_degrees,
                                     image_format=options["format"])
        return True

    if options["time_series"]:
        imgs_local_path = os.path.join(sweet_local_path, "times_series")
        imgs_full_path = os.path.abspath(imgs_local_path)
        Common.mkdir(imgs_full_path)
        TimeSeriesPlotlib.create(full_path, imgs_full_path, imgs_local_path,
                                 show_in_degrees=show_in_degrees,
                                 image_format=options["format"])
        return True

    if options["avi"]:
        AVI.create(full_path, sweet_full_path, sweet_local_path,
                   options["avi"], options["scale"], "answer",
                   show_in_degrees=show_in_degrees)
        return True

    if options["statistics_on_terminal"]:
        MeshProperties.display_statistics(full_path)
        return True

    return
