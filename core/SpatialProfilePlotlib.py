from matplotlib import pyplot as plt
import numpy as np
import os

import FileLoader
import Default
import Common

if Default.DEBUG_MODE:
    from IPython import embed

###############################################################################
# Get color properties
###############################################################################
c_ref = Default.reference_values_color
c_ans = Default.answer_values_color
lw_ref = Default.reference_values_lw
lw_ans = Default.answer_values_lw
PLOT_DW = Default.minimum_allowed_difference_for_plot
use_common_scale = Default.use_common_scale

###############################################################################
# Plot all data contained in Spatial Profile
###############################################################################
def create(data_path, imgs_path, imgs_local_path,
           show_in_degrees=False,
           image_format = "png"):
    """
    Plots the Time Series data for the given folder
    """
    output_path = os.path.join(data_path, "Output")
    answer_path = os.path.join(data_path, "ANSWER")
    # Load configuration
    conf_dic = FileLoader.load_spatial_profile_config(output_path, "SpatialProfilesConfiguration.txt",
                                                      show_in_degrees)
    t_l, x_l, y_l = conf_dic["t"], conf_dic["x"], conf_dic["y"]
    # Check if data at all to plot
    if len(t_l)==0 or len(x_l)==0 or len(y_l)==0:
        print "No data to display as Spatial Profile"
        return

    # Load the engine
    dd = {}
    FileLoader.load_engine(dd, answer_path)
    engine = dd["engine"]
    # Load simulation values
    t_ans, W_ans = FileLoader.load_spatial_profile_data(answer_path, "SpatialProfilesValues.txt")
    # Load true values, if any
    t_ref, W_ref = FileLoader.load_spatial_profile_data(output_path, "SpatialProfilesReferenceValues.txt")

    # START OF DIRTY FIX
    B = np.loadtxt(os.path.join(data_path,"InitialConditions","Bathymetry.txt"))
    ElemNodes = np.loadtxt(os.path.join(data_path,"Mesh","ElemNodes.txt"), dtype=int)
    B_nodes = B[ElemNodes]
    B_cell = B_nodes.sum(axis=1)/3
    SP_cells = np.loadtxt(os.path.join(data_path,"Output","SpatialProfilesConfiguration.txt"),
                          dtype=int, skiprows=3, usecols=(2,))
    W_ground = B[SP_cells]
    # END OF DIRTY FIX

    # Compute min-max for better looking plots
    Wmin, Wmax = Common.get_minmax(W_ans, W_ref)
    #Wmin = min(W_ground.min(), Wmin)
    #print "!!! Wmin, Wmax fixed by hand to specific problem"
    Wmin, Wmax = -1.5, 1.5

    # Get the length of the path of the spatial profile, counting from start
    path_length = [0.0]
    for i in xrange(len(x_l)-1):
        path_length.append(( (x_l[i+1]-x_l[i])**2 + (y_l[i+1]-y_l[i])**2 )**.5 )
    path_length = np.array(path_length).cumsum()
    # Do the plots
    common_measures = max(len(t_ref), len(t_ans))
    for i in range(common_measures):
        print "Generating file %d/%d" %(i+1, common_measures)
        figname = os.path.join(imgs_path, "SpatialProfiles%03d.%s"%(i,image_format))
        generate_plot(i, t_l,
                      t_ref, path_length, W_ref,
                      t_ans, path_length, W_ans,
                      path_length.min(), path_length.max(),
                      Wmin, Wmax, engine,
                      figname,
                      use_common_scale, show_in_degrees, W_ground)
    # Help
    print "Files have been generated on folder %s" %imgs_local_path
    print "One possible visualization"
    if image_format=="png":
        print "\t eog %s" %os.path.join(imgs_local_path, "SpatialProfiles*.png")
    if image_format=="pdf":
        print "\t evince %s" %os.path.join(imgs_local_path, "SpatialProfiles*.pdf")
    return

###############################################################################
# Plot specific data of Spatial Profile
###############################################################################
def generate_plot(i, t,
                  t_ref, d_ref, W_ref,
                  t_ans, d_ans, W_ans,
                  dmin, dmax,
                  Wmin, Wmax, engine,
                  figname,
                  use_common_scale, show_in_degrees, W_ground):
    plt.figure(figsize=(12,8))
    d_unit, d_scale = "m", 1.
    if (dmax-dmin)/1000. > 10.:
        d_unit, d_scale = "km", 1000.
    # Plot the data
    if len(W_ans)>i:
        label_ans = "answer at t=%.2f (%s)" %(t_ans[i], engine)
        #plt.plot(d_ans/d_scale, W_ans[i], c_ans, lw=lw_ans, label=label_ans)
        plt.plot(d_ans/d_scale, W_ans[i], "b", lw=lw_ans, label="Water Level")
        plt.plot(d_ans/d_scale, W_ground, "k", lw=lw_ans, label="Bathymery")
    if len(W_ref)>i:
        label_ref = "reference at t=%.2f" %(t_ref[i])
        plt.plot(d_ref/d_scale, W_ref[i], c_ref, lw=lw_ref, label=label_ref)
    # Fix the scale
    dd = 0.05*(dmax-dmin)
    plt.xlim([(dmin-dd)/d_scale, (dmax+dd)/d_scale])
    if use_common_scale:
        dW = 0.05*(Wmax-Wmin)
        dW = dW if dW>PLOT_DW else PLOT_DW
        plt.ylim([Wmin-dW, Wmax+dW])
    else:
        ymin,ymax = W_ans[i].min(), W_ans[i].max(),
        dy = 0.05*(ymax-ymin)
        plt.ylim(ymin-dy, ymax+dy)
    # Put info on plot
    #plt.legend(loc="upper right")
    plt.legend(loc="lower right")
    plt.ylabel("Water Level [m]")
    if show_in_degrees:
        plt.xlabel("Spatial profile path in degrees")
        plt.title("Time: %.2f [min]"%(t[i]/60.))
    else:
        plt.xlabel("Spatial profile path in [%s]" %d_unit)
        plt.title("Time approx %.2f [min]"%(t[i]/60.))
    plt.grid('on')
    # Save and close
    plt.savefig(figname, bbox_inches='tight', dpi=100)
    plt.close()
