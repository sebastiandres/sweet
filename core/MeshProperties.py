import FileLoader
import DebugMesh
import Common

from matplotlib import pyplot as plt
import numpy as np
import sys
import os

from IPython import embed

INTERACTIVE_PLOT = False

################################################################################
################################################################################
def display_statistics(data_path,
                       interactive=INTERACTIVE_PLOT):
    """
    Compute mesh statistics from provided path.
    Folder must be in QAS format.
    """

    mesh_files = ["NodeCoords.txt", "ElemNodes.txt", "GhostCells.txt"]
    ic_files = ["Bathymetry.txt"]
    question_files = ["WaterLevel.txt","DischargeX.txt", "DischargeY.txt"]

    # The paths where to look for the files
    mesh_path = os.path.join(data_path, "Mesh")
    m2d_path = os.path.join(mesh_path, "Optional")
    ic_path = os.path.join(data_path, "InitialConditions")

    # We will load all the data on the dic data_dic
    data_dict = {}

    # Load Mesh if files are present
    if Common.check_files(mesh_path, mesh_files):
        print "\tLoading Mesh files"
        FileLoader.load_mesh(data_dict, mesh_path, *mesh_files)
        data_dict["ElemNeighs"] = np.loadtxt(os.path.join(mesh_path, "ElemNeighs.txt"), dtype=int)
        compute_regular_cells(data_dict)
        compute_triangle_quality(data_dict)
        # Load Bathymetry if files are present
        if Common.check_files(ic_path, ic_files) and Common.check_files(ic_path, question_files):
            FileLoader.load_bathymetry(data_dict, ic_path, *ic_files)
            FileLoader.load_question_data(data_dict, ic_path, *question_files)
            compute_courant(data_dict)

        # Now print/display the information
        xmin, xmax, ymin, ymax = Common.fast_bbox(data_dict)
        print("\nGENERAL INFORMATION")
        print("\tTotal number of Nodes: {0:,d}".format(data_dict["NodeCoords"].shape[0]))
        print("\tTotal number of Cells: {0:,d}".format(data_dict["ElemNodes"].shape[0]))
        print("\tX in range: [{0:.2E},{1:.2E}] meters".format(xmin, xmax))
        print("\tX direction spans over {0:.2E} kilometers".format((xmax-xmin)*1E-3))
        print("\tY in range: [{0:.2E},{1:.2E}] meters".format(ymin, ymax))
        print("\tY direction spans over {0:.2E} kilometers".format((ymax-ymin)*1E-3))
        ########################################################################
        NCells = data_dict["ElemNodes"].shape[0] # Cast to float to make all divisions float
        NGhostCells = data_dict["GhostCells"].shape[0]
        NRegularCells = NCells-NGhostCells
        print("\nREGULAR AND GHOST CELLS")
        print("\tRegular cells: {0:,d} ({1:,.2f}%)".format(NRegularCells, 100.*NRegularCells/NCells))
        print("\tGhost cells: {0:,d} ({1:,.2f} %)".format(NGhostCells, 100.*NGhostCells/NCells))
        ########################################################################
        print("\nWET AND DRY CELLS")
        NDryCells = data_dict["DryCells"].sum()
        NWetCells = NCells-NDryCells
        print("\tWet cells: {0:,d} ({1:,.2f}%)".format(NWetCells, 100.*NWetCells/NCells))
        print("\tDry cells: {0:,d} ({1:,.2f}%)".format(NDryCells, 100.*NDryCells/NCells))
        ########################################################################
        print("\nTRIANGLE QUALITY")
        print("\tShould be in the range [0.6, 1.0]")
        NBadCells =  sum(data_dict["TriangleQuality"]<0.6)
        describe(data_dict["TriangleQuality"], "Triangle quality")
        print("\tOBS: There are {0:,d} triangles with quality less than 0.6  ({1:.2f}%)".format(NBadCells, 100.*NBadCells/NCells))
        np.set_printoptions(formatter={'float': lambda x: format(x, '6.3E')})
        #print data_dict["TriangleQuality"][:20]
        ########################################################################
        print("\nTime Step based on Courant Restriction [in seconds]")
        describe(data_dict["CourantNumber"], "Courant Number")
        nonNan = np.logical_not(np.isnan(data_dict["CourantNumber"]))
        dt = (data_dict["CourantNumber"][nonNan]).min()
        print("\tOBS: Initial dt should be {0:.5E} [sec]".format(dt))
        #print data_dict["CourantNumber"][:20]

        # Print the obtained ratios
        Tj = data_dict["Tj"]
        rj = data_dict["rjk"]
        print("\nRATIOS FOR AREA:")
        print("\tMax area to min area: {0:,.1f}".format(Tj.max()/Tj.min()))
        print("\tMin area : {0:,.1f} square meters".format(Tj.min()))
        print("\tMax area : {0:,.1f} square meters".format(Tj.max()))
        print("\nRATIOS FOR CELL EDGES:")
        print("\tMax edge to min edge: {0:,.1f}".format(rj.max()/rj.min()))
        print("\tMin edge : {0:,.1f} meters".format(rj.min()))
        print("\tMax edge : {0:,.1f} meters".format(rj.max()))
        # Optional plot
        if interactive:
            quality = data_dict["TriangleQuality"]
            courant = data_dict["CourantNumber"]
            plt.figure(figsize=(12,8))
            ax = plt.subplot(3,1,1)
            plt.plot(quality, courant, 'o', alpha=0.6)
            xlim = ax.get_xlim()
            dx = .1*(xlim[1]-xlim[0])
            ax.set_xlim([xlim[0]-dx,xlim[1]+dx])
            ylim = ax.get_ylim()
            dy = .1*(ylim[1]-ylim[0])
            ax.set_ylim([ylim[0]-dy,ylim[1]+dy])
            plt.xlabel("Triangle Quality")
            plt.ylabel("Courant Number")
            plt.subplot(3,1,2)
            plt.hist(quality, color="g", normed=1, histtype='bar', rwidth=0.8, alpha=0.6)
            plt.ylabel("Triangle Quality")
            plt.subplot(3,1,3)
            aux_courant = courant[np.logical_not(data_dict["DryCells"])]
            plt.hist(aux_courant, color="r", normed=1, histtype='bar', rwidth=0.8, alpha=0.6)
            plt.ylabel("Courant Number")
            plt.show()
    return sys.exit(0)

################################################################################
# Separate the Ghost Cells from the Regular Cells
################################################################################
def describe(my_array, array_name):
    non_nan_array = my_array[np.logical_not(np.isnan(my_array))]
    N = len(non_nan_array)
    print("\tThe {0:,d} Non NAN values have the following properties:".format(N))
    print("\tminimum: {0:.9f}".format(non_nan_array.min()))
    print("\tmean   : {0:.9f}".format(non_nan_array.mean()))
    #print("\tstd    : {0:.5f}".format(non_nan_array.std()))
    print("\tmaximum: {0:.9f}".format(non_nan_array.max()))
    return

################################################################################
# Separate the Ghost Cells from the Regular Cells
################################################################################
def compute_regular_cells(data_dic):
    not_Ghost_cells = data_dic["ElemNodes"].shape[0] - data_dic["GhostCells"].shape[0]
    data_dic["RegularCells"] = np.arange(not_Ghost_cells)
    return

################################################################################
# compute the element properties
################################################################################
def compute_element_properties(data_dict):
    # edge lenghts for each triangle
    ljk = get_element_edge_lengths(data_dict["ElemNodes"].T, data_dict["NodeCoords"].T)
    # Areas for each triangle
    Tj  = get_element_areas(ljk)
    # edge altitudes for each triangle
    rjk = get_element_edge_altitudes(Tj, ljk)
    data_dict["ljk"] = ljk
    data_dict["Tj"] = Tj
    data_dict["rjk"] = rjk
    return

################################################################################
# Compute the courant
################################################################################
def compute_courant(data_dict):
    g = 9.81
    if ("ljk" not in data_dict) or ("Tj" not in data_dict) or ("tjk" not in data_dict):
        compute_element_properties(data_dict)
    ljk = data_dict["ljk"]
    rjk = data_dict["rjk"]
    Tj = data_dict["Tj"]
    Hj = data_dict["W"].copy() - data_dict["B"].copy()
    dry_cells = (Hj<=0)
    Hjk = Hj[data_dict["ElemNeighs"].T]
    a1 = np.maximum(0., np.sqrt( g * np.maximum(Hj, Hjk[0,:]) ))
    a2 = np.maximum(0., np.sqrt( g * np.maximum(Hj, Hjk[1,:]) ))
    a3 = np.maximum(0., np.sqrt( g * np.maximum(Hj, Hjk[2,:]) ))
    r1 = rjk[0,:]
    r2 = rjk[1,:]
    r3 = rjk[2,:]
    courant_j = 0.25 * np.array([r1/a1, r2/a2, r3/a3]).min(axis=0)
    # After taking the min, we can replace the infinities by nan to better plot
    courant_j[courant_j==np.inf] = np.nan
    data_dict["DryCells"] = dry_cells
    data_dict["CourantNumber"] = courant_j
    # Fix bathymetry and water surface in ghost cells
    GC = data_dict["GhostCells"]
    if GC.shape[0]>0:
        data_dict["CourantNumber"][GC[:,0]] = data_dict["CourantNumber"][GC[:,1]]
    return

################################################################################
# Mesh quality
################################################################################
def compute_triangle_quality(data_dict):
    """
    Computes triangle quality using the same formula as in Matlab's pdetriq.
    """
    if ("ljk" not in data_dict) or ("Tj" not in data_dict) or ("tjk" not in data_dict):
        compute_element_properties(data_dict)
    ljk = data_dict["ljk"] # edge sides
    rjk = data_dict["rjk"] # edge altitudes
    Tj = data_dict["Tj"] # triangle area
    q = 4.*Tj*np.sqrt(3.) / (np.power(ljk,2).sum(axis=0))
    data_dict["TriangleQuality"] = q
    return

################################################################################
# COPIED DIRECTLY FROM ANSWER/PYCORE
################################################################################
def get_element_edge_lengths(Elem, mesh_points):
  """
  Gets the element edge sides and returns in order
  Inputs   :
    Elem   : Conectivity array of Elements, 	   [3, nElem]
    Points : Node coordinates array of each node,  [2, nNode]
  Outputs  :
    ljk    : Side lengths for each triangle,       [3, nElem]
  """
  # Small wrapper for DRYing the code
  def length(Elem, mesh_poins, i, j):
    u = mesh_points[:,Elem[i,:]] - mesh_points[:,Elem[j,:]]
    return np.sqrt(u[0,:]**2 + u[1,:]**2)

  #Tangent Vector side: (12)
  L1 = length(Elem, mesh_points, 0, 1)

  #Tangent Vector side: (12)
  L2 = length(Elem, mesh_points, 1, 2)

  #Tangent Vector side: (12)
  L3 = length(Elem, mesh_points, 2, 0)

  ljk = np.array([L1,L2,L3])

  return ljk

################################################################################
# COPIED DIRECTLY FROM ANSWER/PYCORE
################################################################################
def get_element_areas(eel):
  """
  Computes the area for each triangle using Heron's formula.
  eel stands for element edge length
  Inputs   :
    ljk    : Side lengths for each triangle,       [3, nElem]
  Outputs  :
    Tj     : Area of the j-th Triangle,            [1, nElem]
  """
  #Semi-perimeter of each triangle:
  s = 0.5 * eel.sum(axis=0)

  # Heron's formula for area: Need to round up to avoid spurious negative numbers
  decimals = 8
  A2 = s * np.round(s-eel[0,:], decimals) * np.round(s-eel[1,:], decimals) * np.round(s-eel[2,:], decimals)

  return np.sqrt(A2)

################################################################################
# COPIED DIRECTLY FROM ANSWER/PYCORE
################################################################################
def get_element_edge_altitudes(element_areas, element_edge_lengths):
  """
  Returns the altitude (in the geometric sense) for each triangular element
  Inputs   :
    Tj     : Area of the j-th Triangle,            [1, nElem]
    ljk    : Side lengths for each triangle,       [3, nElem]
  Outputs  :
    rjk    : Altitudes at mid-pints for each element.  [3, nElem]
  """
  #Altitudes to each side:
  rjk = np.divide(2*element_areas, element_edge_lengths)
  return rjk
