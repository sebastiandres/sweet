import MeshProperties
import FileLoader
import Common
import GMSH

import numpy as np
import sys
import os

SHOW_NODE_NUMBER = 0

################################################################################
# Loads and saves the corresponding mesh in gmsh format
################################################################################
def create(data_path, sweet_path, usage_path,
           show_in_degrees=False,
           zoom_window=None):
    # The required files
    mesh_files = ["NodeCoords.txt", "ElemNodes.txt", "GhostCells.txt"]
    ic_files = ["Bathymetry.txt"]
    question_files = ["WaterLevel.txt","DischargeX.txt", "DischargeY.txt"]
    m2deg_file = "Meter2Degrees.txt"

    # The paths where to look for the files
    mesh_path = os.path.join(data_path, "Mesh")
    m2d_path = os.path.join(mesh_path, "Optional")
    ic_path = os.path.join(data_path, "InitialConditions")

    # We will load all the data on the dic data_dic
    data_dic = {}

    # Load the Meter2Degrees
    Meter2Degrees = FileLoader.load_meter_to_degrees(m2d_path, m2deg_file, True) # Load always
    # Load Mesh if files are present
    if Common.check_files(mesh_path, mesh_files):
        print "\tLoading Mesh files"
        FileLoader.load_mesh(data_dic, mesh_path, *mesh_files,
                             Meter2Degrees=Meter2Degrees,
                             show_in_degrees=show_in_degrees)
        data_dic["ElemNeighs"] = np.loadtxt(os.path.join(mesh_path, "ElemNeighs.txt"), dtype=int)
        MeshProperties.compute_regular_cells(data_dic)
        MeshProperties.compute_triangle_quality(data_dic)
        # Load Bathymetry if files are present
        if Common.check_files(ic_path, ic_files) and Common.check_files(ic_path, question_files):
            FileLoader.load_bathymetry(data_dic, ic_path, *ic_files)
            FileLoader.load_question_data(data_dic, ic_path, *question_files)
            MeshProperties.compute_courant(data_dic)
        else:
            print "Bathymetry files could not be found."
            print "Courant Number will not be computed."
            return
    else:
        print "Mesh data files could not be found."
        return sys.exit(-1)

    # Save point to show Meter2Degrees, if any
    if Meter2Degrees is None:
        data_dic["m2d_points"] = None
    else:
        if show_in_degrees:
            data_dic["m2d_points"] = Meter2Degrees[:,2:]
        else:
            data_dic["m2d_points"] = Meter2Degrees[:,:2]

    # If zoom, just take a few points
    if zoom_window is not None:
        nan_key_list = ["CourantNumber", "TriangleQuality"]
        Common.update_dict_with_zoom(data_dic, zoom_window, nan_key_list)

    generate_files(data_dic, sweet_path, usage_path)
    return sys.exit(0)


################################################################################
#
################################################################################
def generate_files(data_dic, full_path, local_path):
    """
    """
    # Name of files, required to be specified here for compatibility
    main_file = "sweet.msh"
    mesh_file = "sweet_mesh.msh"
    created_files = []

    # Start the dict with options to be saved as main file
    views_options = []
    visual_options_dic = {}
    visual_options_dic["General"] = ['TrackballHyperbolicSheet = 1',
                                     'TrackballQuaternion0 = 0',
                                     'TrackballQuaternion1 = 0',
                                     'TrackballQuaternion2 = 0',
                                     'TrackballQuaternion3 = 1']
    # Create the mesh
    mesh_options = GMSH.generate_mesh(full_path, mesh_file, data_dic)
    # Overwrite the options
    mesh_options = ['Points = 0',
                    'ColorCarousel=0', # Node and Element number in black
                    'SurfaceNumbers=%d' %SHOW_NODE_NUMBER,
                    'SurfaceEdges = 0',
                    'VolumeEdges = 0']
    created_files.append(mesh_file)

    # Create the Regular Cells file
    RC_file = "RC.msh"
    generate_regular_cells_file(full_path, RC_file, data_dic, views_options)
    created_files.append(RC_file)

    # Create the Ghost Cells file
    GC_file = "GC.msh"
    generate_ghost_cells_file(full_path, GC_file, data_dic, views_options)
    created_files.append(GC_file)

    # Create the Courant file
    C_file = "Courant.msh"
    generate_courant_file(full_path, C_file, data_dic, views_options)
    created_files.append(C_file)

    # Create the Courant file
    Q_file = "MeshQuality.msh"
    generate_quality_file(full_path, Q_file, data_dic, views_options)
    created_files.append(Q_file)

    # Add the Meter2Degrees file
    m2d_file = "Meter2Degrees.msh"
    generate_table(full_path, m2d_file, data_dic, views_options)
    created_files.append(m2d_file)

    # Save the main file
    visual_options_dic.update({"Mesh":mesh_options, "View":views_options})
    generate_main_file(main_file, visual_options_dic, created_files, full_path)

    # Suggest how to use
    GMSH.usage(local_path, main_file)

    return sys.exit(0)

################################################################################
# Generates the main file
################################################################################
def generate_main_file(main_file, main_dic, myfiles, full_path):
    # Write file
    fh = open(os.path.join(full_path, main_file), "w")
    for myfile in myfiles:
        fh.write('Merge "%s";\n' %myfile)
    # Print Mesh options
    for opt in main_dic["Mesh"]:
        fh.write('Mesh.%s;\n' %opt)
    for opt in main_dic["General"]:
        fh.write('General.%s;\n' %opt)
    # Print options Mesh Surface
    for i, opt_list in enumerate(main_dic["View"]):
        for opt in opt_list:
            fh.write('View[%d].%s;\n' %(i, opt))
    # Close it
    fh.close()
    return

################################################################################
# Generate the regular cells
################################################################################
def generate_regular_cells_file(full_path, RC_file, data_dic, views):
    print "\tWriting Regular Cells file"
    fh = open(os.path.join(full_path, RC_file), "w")
    GMSH.write_format(fh)
    # Cells to skip or where to put nan
    ghost_cells = GMSH.get_ghost_cell_list(data_dic["GhostCells"])
    unwanted_cells = []
    # Write the values
    RC_data = np.zeros(data_dic["ElemNodes"].shape[0])
    GMSH.writeElementData(fh, RC_data, 0.0, 0, ghost_cells, unwanted_cells, "Regular Cells",
                          delete_ghost_cells_bool=True,
                          delete_unwanted_cells_bool=True)
    fh.close()
    # Graphical options
    options = ['Axes = 1',
               'Visible = 0',
               'ColormapNumber = 1',
               'ColormapInvert = 0',
               'ColormapSwap = 0',
               'ShowElement = 0',
               'DrawStrings = 0',
               'ShowScale = 0',
               'VectorType = 5',
               'IntervalsType = 3',
               'NbIso = 20',
               ]
    views.append(options)
    return

################################################################################
# Generate the Ghost cells
################################################################################
def generate_ghost_cells_file(full_path, RC_file, data_dic, views):
    print "\tWriting Ghost Cells file"
    fh = open(os.path.join(full_path, RC_file), "w")
    GMSH.write_format(fh)
    # Cells to skip or where to put nan
    ghost_cells = []
    unwanted_cells = data_dic["RegularCells"]
    # Write the values
    GC = data_dic["GhostCells"]
    GC_data = np.zeros(data_dic["ElemNodes"].shape[0])
    if GC.shape[0]>0:
        GC_data[GC[:,0]] = GC[:,2]
    GMSH.writeElementData(fh, GC_data, 0.0, 0, ghost_cells, unwanted_cells, "Ghost Cells",
                          delete_ghost_cells_bool=True,
                          delete_unwanted_cells_bool=True)
    fh.close()
    # Graphical options
    options = ['Axes = 1',
               'Visible = 0',
               'ColormapNumber = 10',
               'ColormapInvert = 0',
               'ColormapSwap = 0',
               'ShowElement = 0',
               'DrawStrings = 0',
               'ShowScale = 1',
               'VectorType = 5',
               'IntervalsType = 3',
               'NbIso = 20',
               'CustomMin = -1',
               'CustomMax = +1',
               'RangeType = 2',
               ]
    views.append(options)
    return

################################################################################
# Generate the Counrant Values
################################################################################
def generate_courant_file(full_path, C_file, data_dic, views):
    print "\tWriting Courant file"
    fh = open(os.path.join(full_path, C_file), "w")
    GMSH.write_format(fh)
    # Cells to skip or where to put nan
    ghost_cells = []
    unwanted_cells = []
    # Write the values
    C_data = data_dic["CourantNumber"]
    GMSH.writeElementData(fh, C_data, 0.0, 0, ghost_cells, unwanted_cells, "Courant",
                          delete_ghost_cells_bool=True,
                          delete_unwanted_cells_bool=True)
    fh.close()
    # Graphical options
    options = ['Axes = 1',
               'Visible = 1',
               'ColormapNumber = 16',
               'ColormapInvert = 0',
               'ColormapSwap = 1',
               'ShowElement = 0',
               'DrawStrings = 0',
               'ShowScale = 1',
               'VectorType = 5',
               'IntervalsType = 3',
               'NbIso = 20',
               ]
    views.append(options)
    return

################################################################################
# Generate the Counrant Values
################################################################################
def generate_quality_file(full_path, Q_file, data_dic, views):
    print "\tWriting Mesh Quality file"
    fh = open(os.path.join(full_path, Q_file), "w")
    GMSH.write_format(fh)
    # Cells to skip or where to put nan
    ghost_cells = []
    unwanted_cells = []
    # Write the values
    Q_data = data_dic["TriangleQuality"]
    GMSH.writeElementData(fh, Q_data, 0.0, 0, ghost_cells, unwanted_cells, "Quality",
                          delete_ghost_cells_bool=True,
                          delete_unwanted_cells_bool=True)
    fh.close()
    # Graphical options
    options = ['Axes = 1',
               'Visible = 0',
               'ColormapNumber = 2',
               'ColormapInvert = 0',
               'ColormapSwap = 0',
               'ShowElement = 0',
               'DrawStrings = 0',
               'ShowScale = 1',
               'VectorType = 5',
               'IntervalsType = 3',
               'NbIso = 20',
               ]
    views.append(options)
    return

################################################################################
# Write Meter to degrees table
################################################################################
def generate_table(full_path, filename, data_dic, views):
    print "\tWriting Meter2Degrees file"
    fh = open(os.path.join(full_path, filename), "w")
    Coords = data_dic["m2d_points"]
    # Check for content
    if Coords is None:
        fh.close()
        return
    # Write the file
    fh.write('View "Meter2Degrees Table" {\n')
    x = Coords[:,0]
    y = Coords[:,1]
    # Write the file
    for xi, yi in zip(x,y):
        fh.write('SP(%.5f,%.5f,%.5f){0.0};\n' %(xi, yi, 0.0))
    fh.write('};\n')
    fh.close()
    # gmsh display options
    options = ['Visible = 0',
               'PointSize = 2',
               'PointType = 0',
               'ShowScale = 0',
               'ColormapNumber = 0']
    views.append(options)
    return
