import numpy as np
import os

import QASFormat as QAS
import GMSHHelpers
import Default
import Common

if Default.DEBUG_MODE:
    from IPython import embed

################################################################################
# WRITE GMSH FILES
################################################################################
def export_to_gmsh(fpath):
    qas_dict = QAS.load(fpath, "maps")
    use_cells = GMSHHelpers.use_cells_in_gmsh(qas_dict)
    generate_gmsh(fpath, qas_dict, use_cells)
    return

################################################################################
# WRITE GMSH FILES
################################################################################
def generate_gmsh(path, qas_dict, use_cells):
    """
    Write the gmsh files.
    """
    sweet_path = os.path.join(path, "SWEET")
    gmsh_path = os.path.join(sweet_path, "gmsh4maps")

    # Make the directories
    print("Creating folders:")
    Common.mkdir(sweet_path)
    Common.mkdir(gmsh_path)

    # Setup previous to create the files
    GMSHHelpers.qas_setup(qas_dict, gmsh_path, use_cells)

    # Initialize the files, views and options.
    created_files = []
    views_options = []
    visual_options_dic = {}

    # Some general values: View from z
    visual_options_dic["General"] = ['RotationX = 0',
                                     'RotationY = 0',
                                     'RotationZ = 0',
                                     'TrackballHyperbolicSheet = 1',
                                     'TrackballQuaternion0 = 0',
                                     'TrackballQuaternion1 = 0',
                                     'TrackballQuaternion2 = 0',
                                     'TrackballQuaternion3 = 1']

    ##############################################################
    # Create the arrival times view
    ##############################################################
    if "ArrivalTimes" in qas_dict:
        view_opt = ['Visible = 1','PointSize = 2','PointType = 10',
                    'Axes = 1', 'AxesTicsX = 2','AxesTicsY = 2',
                    'ShowScale = 1',
                    'ColormapNumber = 16','ColormapInvert = 0','ColormapSwap = 1']
        views_options.append(view_opt)
        unwanted_cells = qas_dict["ArrivalTimes"] < 0
        created_file = GMSHHelpers.write_values(gmsh_path, "arrival_times",
                                qas_dict, "ArrivalTimes",
                                "Arrival Times [s]", scale=0,
                                unwanted_cells = unwanted_cells,
                                use_cells=use_cells
                                )
        created_files.append(created_file)

    ##############################################################
    # Variables setup
    ##############################################################
    if "MaxWaterHeights" in qas_dict:
        qas_dict["Floading"] = qas_dict["MaxWaterHeights"] - qas_dict["Bathymetry"]

    ##############################################################
    # Create the Heat Map
    ##############################################################
    if "MaxWaterHeights" in qas_dict:
        view_opt = ['Visible = 0','PointSize = 2','PointType = 10',
                    'ShowScale = 1','Axes = 1','AxesTicsX = 2','AxesTicsY = 2',
                    'ColormapNumber = 7','ColormapInvert = 0','ColormapSwap = 1']
        views_options.append(view_opt)
        #unwanted_cells = qas_dict["InitialDryCells"]
        unwanted_mask = qas_dict["Floading"] < 1E-2
        created_file = GMSHHelpers.write_values(gmsh_path, "heat_map",
                                qas_dict, "MaxWaterHeights",
                                "HeatMap [m]", scale=0,
                                unwanted_mask = unwanted_mask,
                                use_cells=use_cells,
                                delete_unwanted_cells_bool=True
                                )
        created_files.append(created_file)

    ##############################################################
    # Create the Inundation Map
    ##############################################################
    if "Floading" in qas_dict:
        view_opt = ['Visible = 0','PointSize = 2','PointType = 10',
                    'ShowScale = 1','Axes = 1', 'AxesTicsX = 2','AxesTicsY = 2',
                    'ColormapNumber = 7', 'ColormapInvert = 0', 'ColormapSwap = 1']
        views_options.append(view_opt)
        unwanted_cells = np.logical_not(qas_dict["InitialDryCells"])
        created_file = GMSHHelpers.write_values(gmsh_path, "floading",
                                qas_dict, "Floading",
                                "Floading [m]", scale=0,
                                unwanted_cells = unwanted_cells,
                                use_cells=use_cells,
                                delete_unwanted_cells_bool=True
                                )
        created_files.append(created_file)

    ###########################################################
    # Update the visual options before saving the data
    ###########################################################
    visual_options_dic.update({"View":views_options})

    # Save generate the main file to be open with gmsh
    main_file = "maps.msh"
    GMSHHelpers.generate_main_file(main_file, visual_options_dic,
                                   created_files, gmsh_path,
                                   use_cells=use_cells,
                                   )

    # Suggest how to use
    GMSHHelpers.usage(gmsh_path, main_file)
    return
