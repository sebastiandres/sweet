import numpy as np
import sys
import os

import Default
import Common

if Default.DEBUG_MODE:
    from IPython import embed

TOL_DRY_CELLS = 1E-6     # THRESHOLD TO CONSIDER A CELL AS DRY
HIDE_DRY_CELLS = True    # HIDE ALL DRY CELLS (AVOID SHOWING THEM)
HIDE_GHOST_CELLS = True  # HIDE GHOST CELLS (AVOID SHOWING THEM)
MAX_PLOTTED_CELL_BOUNDARIES = 5000 # LIMIT TO SHOW ELEMENT BOUNDARIES

################################################################################
# Print usage help
################################################################################
def usage(path, main_file):
    print 'Visualize with gmsh:'
    print '\tgmsh "%s" &' %(os.path.join(path, main_file))
    return

################################################################################
# KNOW WHEN TO USE CELLS
################################################################################
def use_cells_in_gmsh(qas_dict):
    return qas_dict["Mesh"].shape[0] < Default.CELLS_TO_POINTS_THRESHOLD

################################################################################
# Generates the main file
################################################################################
def generate_main_file(main_file, main_dic, myfiles, full_path, use_cells=False):
    # Write file
    fh = open(os.path.join(full_path, main_file), "w")
    if use_cells:
        fh.write('Merge "%s";\n' %"mesh.msh")
    for myfile in myfiles:
        fh.write('Merge "%s";\n' %myfile)
    for opt in main_dic["General"]:
        fh.write('General.%s;\n' %opt)
    if use_cells:
        fh.write('Mesh.SurfaceEdges = 0;\n')
        fh.write('Mesh.VolumeEdges = 0;\n')
    # Print options Mesh Surface
    for i, opt_list in enumerate(main_dic["View"]):
        for opt in opt_list:
            fh.write('View[%d].%s;\n' %(i, opt))
    # Close it
    fh.close()
    return

################################################################################
# WRITE GMSH FILES: POINTS OR ELEMENTS DEPENDING ON SIZE
################################################################################
def qas_setup(qas_dict, gmsh_path, use_cells=False):
    N = qas_dict["Mesh"].shape[0]
    if use_cells:
        # Will render with cells. Needs to save the Mesh configuration.
        write_mesh(qas_dict, gmsh_path)
    else:
        # Will render with points. Compute the centroids
        Mesh = qas_dict["Mesh"]
        elementNodes = qas_dict["ElementNodes"]
        qas_dict["Centroids"] = Mesh[elementNodes].mean(axis=1)
    return

################################################################################
# WRITE MESH FILE
################################################################################
def write_mesh(qas_dict, gmsh_path):
    print "\tWriting Mesh file"
    filename = os.path.join(gmsh_path, "mesh.msh")
    fh = open(filename, "w")
    write_format(fh)
    # Params
    nNodes = qas_dict["Mesh"].shape[0]
    nElems = qas_dict["ElementNodes"].shape[0]
    # Write the nodes
    fh.write('$Nodes\n')
    fh.write('%d\n'%nNodes)
    for i, (xi, yi) in enumerate(qas_dict["Mesh"]):
        fh.write("%d %.5f %.5f %.5f\n" %(i+1, xi, yi, 0.0))
    fh.write('$EndNodes\n')
    # Write the elements
    elem_type = 2
    tags = 0
    fh.write('$Elements\n')
    fh.write('%d\n'%nElems)
    for j, (n0, n1, n2) in enumerate(qas_dict["ElementNodes"]):
        fh.write("%d %d %d %d %d %d\n" %(j+1, elem_type, tags, n0+1, n1+1, n2+1))
    fh.write('$EndElements\n')
    # Close
    fh.close()
    # Mesh options
    options = ["SurfaceEdges = 0", "VolumeEdges = 0"]
    return options


################################################################################
# Helper
################################################################################
def write_format(fh):
    fh.write('$MeshFormat\n2.2 0 8\n$EndMeshFormat\n')
    return

################################################################################
# WRITE GMSH FILES: POINTS OR ELEMENTS DEPENDING ON SIZE
################################################################################
def write_values(full_path, filename,
                 qas_dict, key,
                 view_name, scale=1.0,
                 ghost_cells=np.array([]),
                 unwanted_cells=np.array([]),
                 unwanted_mask=np.array([]),
                 delete_ghost_cells_bool=HIDE_GHOST_CELLS,
                 delete_unwanted_cells_bool=HIDE_DRY_CELLS,
                 use_cells=False,
                 ):
    """
    Writes the values using points or cells.
    """
    N = np.max(qas_dict[key])
    N = 1E9
    if use_cells:
        filename += ".msh"
        raw_data = qas_dict[key]
        t_float = 0.0
        t_int = 0
        points = qas_dict["Mesh"]
        write_data_on_mesh(full_path, filename,
                             raw_data, t_float, t_int,
                             view_name, scale,
                             ghost_cells=ghost_cells,
                             unwanted_cells=unwanted_cells,
                             unwanted_mask=unwanted_mask,
                             delete_ghost_cells_bool=delete_ghost_cells_bool,
                             delete_unwanted_cells_bool=delete_unwanted_cells_bool)
    else:
        filename += ".pos"
        points = qas_dict["Centroids"]
        values = qas_dict[key]
        write_data_points(full_path, filename,
                          points, values,
                          view_name, scale,
                          ghost_cells,
                          unwanted_cells,
                          unwanted_mask,
                          delete_ghost_cells_bool,
                          delete_unwanted_cells_bool)
    return filename

################################################################################
# WRITE GMSH FILES USING POINTS
################################################################################
def write_data_points(full_path, filename,
                      points, heights,
                      view_name, scale,
                      ghost_cells=np.array([]),
                      unwanted_cells=np.array([]),
                      unwanted_mask=np.array([]),
                      delete_ghost_cells_bool=HIDE_GHOST_CELLS,
                      delete_unwanted_cells_bool=HIDE_DRY_CELLS):
    """
    Writes the points at the provided heights in gmsh format.
    View is not based in mesh, so it can be rendered directly.
    We might need to skip ghost cells or unwanted cells, if
    the mesh is too big (it will be rendered with points).
    """
    print("\tWriting file {0}".format(filename))
    # Check for content
    if points.shape[0]==0:
      return
    # We know we have something to write
    fh = open(os.path.join(full_path, filename), "w")
    fh.write('View "' + view_name +'" {\n')
    x = points[:,0]
    y = points[:,1]
    z = heights
    # Check values to delete
    delete_values = np.isnan(z)
    # Check ghost cells
    if delete_ghost_cells_bool and len(ghost_cells)>0:
        delete_values[ghost_cells] = True
    # Check unwanted cells
    if delete_unwanted_cells_bool and len(unwanted_cells)>0:
        delete_values[unwanted_cells] = True
    # Check unwanted mask
    if delete_unwanted_cells_bool and len(unwanted_mask)>0:
        delete_values = np.logical_or(delete_values, unwanted_mask)
    # Keep only the important values
    keep_values = np.logical_not(delete_values)
    x = x[keep_values]
    y = y[keep_values]
    z = z[keep_values]
    # Save the values
    for xi, yi, zi in zip(x,y,z):
        fh.write('SP(%.5f,%.5f,%.5f){%.5f};\n' %(xi, yi, scale*zi, zi))
    fh.write('};\n')
    fh.close()
    return

################################################################################
# Write Element Data for file
################################################################################
def write_data_on_mesh(full_path, filename,
                       raw_data, t_float, t_int,
                       view_name, scale,
                       ghost_cells=np.array([]),
                       unwanted_cells=np.array([]),
                       unwanted_mask=np.array([]),
                       delete_ghost_cells_bool=HIDE_GHOST_CELLS,
                       delete_unwanted_cells_bool=HIDE_DRY_CELLS):
    """
    Writes the data on the existent mesh.
    Usually, cells_to_erase will be the dry cells, but we might want to erase wet cells as well.
    """
    print("\tWriting file {0}".format(filename))
    fh = open(os.path.join(full_path, filename), "w")
    write_format(fh)
    # Detect shape
    if len(raw_data.shape)==1:
         nElems, nrows = raw_data.shape[0], 1
    else:
         nElems, nrows = raw_data.shape
    # Now work with the data
    data = raw_data.copy() # to avoid overwrite the loaded data and have collateral surprises
    # Check values to delete
    delete_values = np.isnan(data)
    # Check ghost cells
    if delete_ghost_cells_bool and len(ghost_cells)>0:
        delete_values[ghost_cells] = True
    # Check unwanted cells
    if delete_unwanted_cells_bool and len(unwanted_cells)>0:
        delete_values[unwanted_cells] = True
    # Check unwanted mask
    if delete_unwanted_cells_bool and len(unwanted_mask)>0:
        delete_values = np.logical_or(delete_values, unwanted_mask)
    # Keep only the important values
    data[delete_values] = np.nan
    # Write the Element Data
    fh.write('$ElementData\n')
    fh.write('1\n')               # One name
    fh.write('"%s"\n' %view_name) # The name for the view
    fh.write('1\n')               # One time step
    fh.write('%.2f\n' %t_float)   # The time step value
    fh.write('3\n')               # 3 numbers follow
    fh.write('%d\n' %t_int)       # The index of the time step
    fh.write('%d\n'%nrows)        # The number of values that will be provided
    fh.write('%d\n' %nElems)      # The number of element values
    if nrows==1:
        for j, val_z in enumerate(data):
    	    line = "%d %.5f\n" %(j+1, val_z)
	    fh.write(line.replace("nan","NaN"))
    elif nrows==2:
        for j, (val_x, val_y) in enumerate(data):
    	    line = "%d %.5f %.5f\n" %(j+1, val_x, val_y)
	    fh.write(line)#.replace("nan","NaN"))
    elif nrows==3:
        for j, (val_x, val_y, val_z) in enumerate(data):
    	    line = "%d %.5f %.5f %.5f\n" %(j+1, val_x, val_y, val_z)
	    fh.write(line)#.replace("nan","NaN"))
    else:
        print "Data not understood"
    fh.write('$EndElementData\n')

    return
