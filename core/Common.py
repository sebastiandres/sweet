import numpy as np
import itertools
import sys
import os

import Default

if Default.DEBUG_MODE:
    from IPython import embed

################################################################################
# Get a scalar value to scale the image to fit nicely into screen
################################################################################
def get_dynamic_scale(mesh, B, terminal_scale):
    """
    Returns a float, used to scale the z-axis for a better rendering in gmsh.
    """
    # Parameters
    default_small_scale = 1E-3
    default_large_scale = 1E3
    # If given, used scale provided by user
    if (terminal_scale is not None) and (terminal_scale>0):
        return terminal_scale
    # Compute the scale
    x, y  = mesh.T
    xy_ptp = max(x.ptp(), y.ptp())
    z_ptp = max(np.abs(B.min()), np.abs(B.ptp()))
    if z_ptp==0:
        # Bathymetry was flat. Assume wave of height 1
        z_ptp = 1
    scale = 0.5 * xy_ptp/z_ptp
    if np.isnan(scale) or np.isinf(abs(scale)):
        print("\tScale not computed correctly. Using 1.0")
        return 1.0
    if scale < default_small_scale:
        print "\tDynamic scale gives %f, using scale %f)" %(scale, default_small_scale)
        print "xy_ptp = %f, z_ptp = %f" %(xy_ptp, z_ptp)
        return default_small_scale
    if scale>default_large_scale:
        print "\tDynamic scale gives %f, using scale %f)" %(scale, default_large_scale)
        print "xy_ptp = %f, z_ptp = %f" %(xy_ptp, z_ptp)
        return default_large_scale
    scale = np.round(scale, decimals=4)
    print "\tUsing scale %.5f" %scale
    return scale


################################################################################
# Zoom: Nan values outside the domain
################################################################################
def update_dict_with_zoom(data_dic, zoom_window, nan_key_list):
    # Compute the centroids
    NodeCoords = data_dic["NodeCoords"]
    ElemNodes = data_dic["ElemNodes"]
    x = NodeCoords[ElemNodes,0]
    y = NodeCoords[ElemNodes,1]
    xj = x.sum(axis=1)/3.
    yj = y.sum(axis=1)/3.
    # Nan the ghost cells, so they are taken out
    first_ghost_cell = data_dic["GhostCells"][:,0].min()
    xj[first_ghost_cell:]  = np.nan
    yj[first_ghost_cell:]  = np.nan
    # Compute the indicators for the window
    if len(zoom_window) not in [1,2,4]:
        print("Zoom must be a range or a value")
        return sys.exit(-1)
    # Zoom in longituda and latitude range
    if len(zoom_window)==4:
        xmin, xmax, ymin, ymax = zoom_window
        print("\nSelecting elements with longitude in range [{0},{1}] (in meters)".format(xmin, xmax))
        print("Selecting elements with latitude in range [{0},{1}] (in meters)".format(ymin, ymax))
        i1 = ( xmin <= xj )
        i2 = ( xj <= xmax )
        i3 = ( ymin <= yj )
        i4 = ( yj <= ymax )
        non_nan_ind = np.logical_and(np.logical_and(i1,i2), np.logical_and(i3,i4))
        # The ones to be nan-ed away
        nan_ind = np.logical_not(non_nan_ind)
    Bj = data_dic["B"]
    Bj1, Bj2, Bj3 = data_dic["B"][data_dic["ElemNeighs"]].T
    change_sign = np.logical_or(Bj*Bj1<=0, Bj*Bj2<=0)
    change_sign = np.logical_or(change_sign, Bj*Bj3<=0)
    if len(zoom_window)==2:
        dz_min = zoom_window[0]
        dz_max = zoom_window[1]
        print("\nSelecting elements with bathymetry in range [{0},{1}] (in meters)".format(dz_min, dz_max))
        non_nan_ind = np.logical_and( dz_min <= data_dic["B"], data_dic["B"] <= dz_max)
        non_nan_ind[change_sign] = True
        # The ones to be nan-ed away
        nan_ind = np.logical_not(non_nan_ind)
    if len(zoom_window)==1:
        dz = float(zoom_window[0])
        print("\nSelecting elements with bathymetry in range [{0},{1}] (in meters)".format(-dz, dz))
        non_nan_ind = np.logical_and( -dz <= data_dic["B"], data_dic["B"] <= dz)
        non_nan_ind[change_sign] = True
        # The ones to be nan-ed away
        nan_ind = np.logical_not(non_nan_ind)
    # Compute which elements are left
    # maps element numbering from the old mesh to the new mesh
    NElems_old = ElemNodes.shape[0]
    NElems_new = non_nan_ind.sum()
    if NElems_new==0:
        print("No Nodes could be selected. Please choose a larger zoom window.")
        sys.exit(-1)
    elem_new_to_old = np.arange(NElems_old)[non_nan_ind]
    elem_old_to_new = dict(itertools.izip(elem_new_to_old, np.arange(NElems_new)))
    # test
    """
    i = elem_old_to_new.keys()[0]
    print i, elem_new_to_old[elem_old_to_new[i]]
    print i, elem_old_to_new[elem_new_to_old[i]]
    """
    # Compute which nodes are left
    # maps node numbering from the old mesh to the new mesh
    NNodes_old = NodeCoords.shape[0]
    new_aux_nodes = np.unique(ElemNodes[non_nan_ind])
    NNodes_new = new_aux_nodes.shape[0]
    node_new_to_old = np.sort(new_aux_nodes)
    node_old_to_new = dict(itertools.izip(node_new_to_old, np.arange(NNodes_new)))
    print("\tFrom {0} to {1} Nodes".format(NNodes_old, NNodes_new))
    print("\tFrom {0} to {1} Elements".format(NElems_old, NElems_new))
    # test
    """
    i = node_old_to_new.keys()[0]
    print i, node_new_to_old[node_old_to_new[i]]
    print i, node_old_to_new[node_new_to_old[i]]
    """
    # Rewire Nodes
    NodeCoords_new = np.zeros([NNodes_new, 2])
    for i_new in xrange(NNodes_new):
        i_old = node_new_to_old[i_new]
        point = NodeCoords[i_old]
        NodeCoords_new[i_new,:] = point
    data_dic["NodeCoords"] = NodeCoords_new
    # Rewire Elements
    ElemNodes_new = np.zeros([NElems_new, 3])
    for i in xrange(NElems_new):
        i1_old, i2_old, i3_old = ElemNodes[elem_new_to_old[i],:]
        i1 = node_old_to_new[i1_old]
        i2 = node_old_to_new[i2_old]
        i3 = node_old_to_new[i3_old]
        ElemNodes_new[i,:] = [i1,i2,i3]
    data_dic["ElemNodes"] = ElemNodes_new
    # No Ghost Cells Now
    data_dic["GhostCells"] = np.array([])
    data_dic["RegularCells"] = np.arange(NElems_new)
    # New Time Series
    if "time_series" in data_dic:
        TS_new = {"cells":[], "x":[], "y":[]}
        TimeSeries_x_new = []
        TimeSeries_y_new = []
        TS_old = data_dic['time_series']
        for cell_old, x_old, y_old in zip(TS_old['cells'], TS_old['x'], TS_old['y']):
            if cell_old in elem_old_to_new:
                TS_new["cells"].append(elem_old_to_new[cell_old])
                TS_new["x"].append(x_old)
                TS_new["y"].append(y_old)
        data_dic['time_series'] = TS_new

    # New Spatial Profile
    # TO BE DONE

    # Rewire everything
    for key in nan_key_list:
        if key not in data_dic:
            continue
        if len(data_dic[key].shape)==2:
            data_dic[key] = data_dic[key][:,non_nan_ind]
        else:
            data_dic[key] = data_dic[key][non_nan_ind]
    return

################################################################################
# Zoom: Nan values outside the domain
################################################################################
def zoom_on_values(data_dic, zoom_window, nan_key_list,
                   keep_ghost_cells=True):
    NodeCoords = data_dic["NodeCoords"]
    ElemNodes = data_dic["ElemNodes"]
    cell_centroids = NodeCoords[ElemNodes].sum(axis=1)/3.
    x = cell_centroids[:,0]
    y = cell_centroids[:,1]
    xmin, xmax, ymin, ymax = zoom_window
    i1 = ( xmin <= x )
    i2 = ( x <= xmax )
    i3 = ( ymin <= y )
    i4 = ( y <= ymax )
    ind = np.logical_and(np.logical_and(i1,i2), np.logical_and(i3,i4))
    nan_ind = np.logical_not(ind)
    for key in nan_key_list:
        data_dic[key][nan_ind] = np.nan
    return

################################################################################
# Zoom: Nan nodes outside the domain
################################################################################
def zoom_on_nodes(data_dic, zoom_window, keep_ghost_cells=True):
    NodeCoords = data_dic["NodeCoords"]
    x = data_dic["NodeCoords"][:,0]
    y = data_dic["NodeCoords"][:,1]
    xmin, xmax, ymin, ymax = zoom_window
    i1 = ( xmin <= x )
    i2 = ( x <= xmax )
    i3 = ( ymin <= y )
    i4 = ( y <= ymax )
    ind = np.logical_and(np.logical_and(i1,i2), np.logical_and(i3,i4))
    zoom_points = data_dic["NodeCoords"].copy()
    zoom_points[np.logical_not(ind)] = np.nan
    data_dic["NodeCoords"] = zoom_points
    return

################################################################################
# Zoom: Nan nodes outside the domain
################################################################################
def fast_bbox(data_dict):
    ElemRegularNodes = data_dict["ElemNodes"][data_dict["RegularCells"]]
    ElemRegularNodes = np.unique(ElemRegularNodes) # Unique and in flat array
    RegularNodes = data_dict["NodeCoords"][ElemRegularNodes]
    # Compute the mins and maxs.
    # Better do it twice in numpy than once in a for loop
    xmin, ymin = RegularNodes.min(axis=0)
    xmax, ymax = RegularNodes.max(axis=0)
    return xmin, xmax, ymin, ymax

################################################################################
# Check if folder exists or create it
################################################################################
def mkdir(folder):
    path = os.path.abspath(folder)
    try:
        os.makedirs(path)
    except OSError:
        if not os.path.isdir(path):
           raise
    return path

################################################################################
# Check if directory exist
################################################################################
def check_dir(folder, quiet=False):
    path = os.path.abspath(folder)
    if os.path.isdir(path):
        return True
    else:
        if not quiet:
            print "%s : Provided folder does not exists" %folder
        return False

################################################################################
# Check if files exist
################################################################################
def check_files(folder, required_files, quiet=False):
    path = os.path.abspath(folder)
    if not check_dir(folder, quiet):
        return False
    available_files  = os.listdir(path)
    all_found = True
    # Check for a unique file string given
    if type(required_files)==str:
        required_files = [required_files]
    # Check for files
    for file in required_files:
        if file not in available_files:
            if not quiet:
                print "%s : File not in provided folder" %file
            all_found = False
    return all_found

################################################################################
# Gets the min and max from a given list v1 and an optionally empty list v2
################################################################################
def list_min(l):
  return min([min(li) for li in l])

def list_max(l):
  return max([max(li) for li in l])

def get_minmax(v1, v2):
    if len(v1)>0 and len(v2)>0:
	vmin = min(list_min(v1), list_min(v2))
	vmax = max(list_max(v1), list_max(v2))
        return vmin, vmax
    if len(v1)>0:
        vmin = list_min(v1)
        vmax = list_max(v1)
        return vmin, vmax
    if len(v2)>0:
        vmin = list_min(v2)
        vmax = list_max(v2)
        return vmin, vmax
    return 0., 0.

################################################################################
#
################################################################################
def empty_file(path):
    """
    Checks the header of the file, which must conform to '# d1 d2'
    and returns True if d1 is 0.
    """
    with open(path, "r") as fh:
        line = fh.readline()
        N = int(line[2:].split(" ")[0])
    return True if N==0 else False
