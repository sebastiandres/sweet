import numpy as np
import os

import SBFFormat as SBF
import GMSHHelpers
import Common

################################################################################
# WRITE GMSH FILES
################################################################################
def export_to_gmsh(fpath, show_in_degrees):
    sbf_dict = SBF.load(fpath, show_in_degrees)
    generate_gmsh(fpath, sbf_dict)
    return

################################################################################
# WRITE GMSH FILES
################################################################################
def generate_gmsh(path, sbf_dict):
    """
    Writes the gmsh files.
    """
    sweet_path = os.path.join(path, "SWEET")
    gmsh_path = os.path.join(sweet_path, "gmsh4sbf")

    # Compute the scale
    scale = Common.get_dynamic_scale(sbf_dict["mesh"], sbf_dict["B"], None)

    # Make the directories
    print("Creating folders:")
    Common.mkdir(sweet_path)
    Common.mkdir(gmsh_path)

    # Initialize the files, views and options.
    created_files = []
    views_options = []
    visual_options_dic = {}

    # Some general values: View from z
    visual_options_dic["General"] = ['RotationX = 0',
                                     'RotationY = 0',
                                     'RotationZ = 0',
                                     'TrackballHyperbolicSheet = 1',
                                     'TrackballQuaternion0 = 0',
                                     'TrackballQuaternion1 = 0',
                                     'TrackballQuaternion2 = 0',
                                     'TrackballQuaternion3 = 1']
    # Separate topography from bathymetry
    bathymetry_mask = sbf_dict["B"]<0.0
    topography_mask = np.logical_not(bathymetry_mask)

    # Create the bathymetry file
    bathymetry_file = "bathymetry.pos"
    bathymetry_view_options = ['Visible = 1','PointSize = 2','PointType = 10',
                          'Axes = 1', 'AxesTicsX = 2','AxesTicsY = 2','AxesTicsZ = 0',
                          'ShowScale = 1','ColormapNumber = 17']
    views_options.append(bathymetry_view_options)
    GMSHHelpers.write_data_points(gmsh_path, bathymetry_file,
                    sbf_dict["mesh"],
                    sbf_dict["B"],
                    "Bathymetry", scale,
                    unwanted_mask = topography_mask,
                    delete_unwanted_cells_bool = True
                    )
    #views_options.append(bathymetry_options) #??
    created_files.append(bathymetry_file)

    # Create the topography file
    topography_file = "topography.pos"
    topography_view_options = ['Visible = 1','PointSize = 2','PointType = 10',
                          'Axes = 1', 'AxesTicsX = 2','AxesTicsY = 2','AxesTicsZ = 0',
                          'ShowScale = 1', 'ColormapNumber = 19']
    views_options.append(topography_view_options)
    GMSHHelpers.write_data_points(gmsh_path, topography_file,
                    sbf_dict["mesh"],
                    sbf_dict["B"],
                    "Topography", scale,
                    unwanted_mask = bathymetry_mask,
                    delete_unwanted_cells_bool = True
                    )
    created_files.append(topography_file)

    # Create the TS buoys file
    if "TimeSeriesBuoyCoordinates" in sbf_dict:
        TS_file = "TimeSeriesBuoyCoordinates.pos"
        TS_height = 0.*sbf_dict["TimeSeriesBuoyCoordinates"][:,0]
        TS_view_options = ['Visible = 1','PointSize = 4','PointType = 1',
                           'ShowScale = 0', 'ColormapNumber = 0']
        views_options.append(TS_view_options)
        GMSHHelpers.write_data_points(gmsh_path, TS_file, sbf_dict["TimeSeriesBuoyCoordinates"],
                        TS_height,
                        "Time Series Buoys", scale,
                        )
        created_files.append(TS_file)

    # Create the TS buoys file
    if "SpatialProfileBuoyCoordinates" in sbf_dict:
        SP_file = "SpatialProfileBuoyCoordinates.pos"
        SP_height = 0.*sbf_dict["SpatialProfileBuoyCoordinates"][:,0]
        SP_view_options = ['Visible = 1','PointSize = 4','PointType = 0',
                           'ShowScale = 0', 'ColormapNumber = 0']
        views_options.append(SP_view_options)
        GMSHHelpers.write_data_points(gmsh_path, SP_file, sbf_dict["SpatialProfileBuoyCoordinates"],
                        SP_height,
                        "Spatial Profile Buoys",  scale,
                        )
        created_files.append(SP_file)

    # Update the visual options before saving the data
    visual_options_dic.update({"View":views_options})

    # Save generate the main file to be open with gmsh
    main_file = "sbf.msh"
    GMSHHelpers.generate_main_file(main_file, visual_options_dic, created_files, gmsh_path)

    # Suggest how to use
    GMSHHelpers.usage(gmsh_path, main_file)
    return
