from scipy.interpolate import griddata
import numpy as np
import pickle
import glob
import sys
import os

import LonLatHandler
import Default

if Default.DEBUG_MODE:
    from IPython import embed

################################################################################
# TESTS FOR SIMPLIFIED FILE FORMAT
################################################################################
def has_valid_format(fpath):
    """
    Check if provided path has the simplified format.
    """
    required_files = ["Bathymetry.txt", "CoordinateSystem.txt"]
    in_simplified_format = all( os.path.exists(os.path.join(fpath, sfile)) for sfile in required_files)
    # OBSERVATION:
    # "TimeSeriesBouyCoordinates.txt" and "SpatialProfileBouyCoordinates.txt" are optional
    return in_simplified_format

################################################################################
# LOAD DATA IN SIMPLIFIED FORMAT
################################################################################
def load(from_path, load_in_degrees):
    """
    Returns the sbf dictionary with the data available in the provided folder.
    """
    print("Reading files in simplified format.")
    sbf_dict = {}
    # Bathymetry
    filename = os.path.join(from_path, "Bathymetry.txt")
    if os.path.exists(filename):
        print("\tLoading Bathymetry file.")
        mesh_and_B = np.loadtxt(filename)
        sbf_dict["mesh"] = mesh_and_B[:,:2]
        sbf_dict["B"] = mesh_and_B[:,2]
        print("\tBathymetry has {0:,} Nodes".format(sbf_dict["B"].shape[0]))
    else:
        print("\t!!! Bathymetry.txt file not found!")
        sys.exit(-1)

    # Coordinate System
    filename = os.path.join(from_path, "CoordinateSystem.txt")
    if os.path.exists(filename):
        print("\tLoading Coordinate System file.")
        CO, LP = load_coordinate_system(filename)
        sbf_dict["CoordinateSystem"] = CO
        if LP is not None:
            sbf_dict["LinearizationPoint"] = LP
    else:
        print("\t!!! CoordinateSystem.txt file not found!")
        sys.exit(-1)

    # Time Series Buoy Coordinates
    filename = os.path.join(from_path, "TimeSeriesBuoyCoordinates.txt")
    if os.path.exists(filename):
        print("\tLoading Time Series Buoy Coordinates.")
        sbf_dict["TimeSeriesBuoyCoordinates"] = np.loadtxt(filename, ndmin=2)
    else:
        print("\t!!! TimeSeriesBuoyCoordinates.txt file not found.")
        print("\t\tAssuming no Time Series Buoys")

    # Spatial Profile Buoy Coordinates
    filename = os.path.join(from_path, "SpatialProfileBuoyCoordinates.txt")
    if os.path.exists(filename):
        print("\tLoading Spatial Profile Buoy Coordinates.")
        sbf_dict["SpatialProfileBuoyCoordinates"] = np.loadtxt(filename, ndmin=2)
    else:
        print("\t!!! SpatialProfileBuoyCoordinates.txt file not found.")
        print("\t\tAssuming no Spatial Profile Buoys")

    # Change to degrees, is possible and asked for
    if load_in_degrees:
        if sbf_dict["CoordinateSystem"]=="cartesian":
            LP = sbf_dict["LinearizationPoint"]
            keys_to_convert = ['mesh', 'TimeSeriesBuoyCoordinates', 'SpatialProfileBuoyCoordinates']
            for key in keys_to_convert:
                if key in sbf_dict:
                    sbf_dict[key] = LonLatHandler.cartesian_to_geographical(sbf_dict[key], LP)
        else:
            print("Data already in degrees (geographical coordinate system)")
    return sbf_dict

################################################################################
# SPECIALIZED READER AND SAVER FOR COORDINATE SYSTEM
################################################################################
def load_coordinate_system(cs_filepath):
    # We asume by default that the file is in cartesian/meters.
    with open(cs_filepath, "r") as filereader:
        for line in filereader.readlines():
            sanitized_line = line.lower().strip()
            if "geographical" in sanitized_line:
                # Coordinate system is geographical, no linearization point
                coordinate_system = "geographical"
                linearization_point = None
                return coordinate_system, linearization_point
            if "linearization point" in sanitized_line:
                # Line should be something as in:
                # '# linearization point (longitude, latitude):288.500000000, -20.000000000'
                coordinate_system = "cartesian"
                LonLat  = sanitized_line.split(":")[1]
                Longitude = float(LonLat.split(",")[0])
                Latitude = float(LonLat.split(",")[1])
                linearization_point = (Longitude, Latitude)
                return coordinate_system, linearization_point
    print "Coordinate System File was not properly read."
    print "\tAssuming geographical Coordinate System"
    return "geographical", None
