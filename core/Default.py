# Set DEBUG_MODE to True to able to call embed() in all files
# Set DEBUG_MODE to False before committing to production

DEBUG_MODE = True

try:
    import IPython
except:
    DEBUG_MODE = False

SHOW_DISCHARGES = False
CELLS_TO_POINTS_THRESHOLD = 5E5

################################################################################
# Configure some plot properties
################################################################################
reference_values_color = "r--"
reference_values_lw = 2.0
answer_values_color = "k"
answer_values_lw = 2.0
minimum_allowed_difference_for_plot = 1.0E-3
use_common_scale = False

FILTER_ALL_WARNINGS = True

if FILTER_ALL_WARNINGS:
    import warnings
    warnings.filterwarnings("ignore")
