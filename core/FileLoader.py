import numpy as np
from scipy import interpolate
import os

import LonLatHandler
import Default
import Common

if Default.DEBUG_MODE:
    from IPython import embed

################################################################################
# Convert to longitude and latitude using interpolation on the provided table
################################################################################
"""
def convert_to_lonlat(new_xy, table, interp_method='linear'):
    old_xy = table[:,:2]
    old_lonlat = table[:,2:]
    new_lonlat = interpolate.griddata(old_xy, old_lonlat, new_xy, method=interp_method, fill_value=0e6)
    return new_lonlat
"""
################################################################################
# Load the Engine
################################################################################
def load_engine(dd, path):
    filename = "Engine.txt"
    try:
        with open(os.path.join(path, filename), "r") as fh:
            engine = fh.readline()[:3]
        dd["engine"] = engine.upper()
    except:
        print "ANSWER has not been executed yet."
        dd["engine"] = ""

"""
################################################################################
# Loads the Meter2Degrees table
################################################################################
def load_meter_to_degrees(path, Meter2DegreesFile,
                          show_in_degrees=False):
    filepath = os.path.join(path, Meter2DegreesFile)
    if os.path.exists(filepath):
        m2deg = np.loadtxt(filepath, skiprows=1, dtype=float)
    else:
        m2deg = None
    return m2deg
"""

################################################################################
# Loads the Mesh values
################################################################################
def load_mesh(dd, path, NodeFile, ElemFile, GhostFile,
              Meter2Degrees=None, show_in_degrees=False):
    dd["NodeCoords"] = np.loadtxt(os.path.join(path, NodeFile), skiprows=1)
    dd["ElemNodes"] = np.loadtxt(os.path.join(path, ElemFile), skiprows=1, dtype=int)
    dd["ElemNeighs"] = np.loadtxt(os.path.join(path, "ElemNeighs.txt"), dtype=int)
    dd["GhostCells"] = np.loadtxt(os.path.join(path, GhostFile), skiprows=1, dtype=int)
    if show_in_degrees:
        print "\tConverting NodeCoords from meters to Longitude/Latitude"
        dd["NodeCoords"] = LonLatHandler.convert_to_lonlat(dd["NodeCoords"])
    return

################################################################################
# Load the bathymetry
################################################################################
def load_bathymetry(dd, path, BFile):
    dd["B"] = np.loadtxt(os.path.join(path, BFile), skiprows=1)
    return

################################################################################
# Loads the bathymetry and water level from question file
################################################################################
def load_question_data(dd, path, WFile, HUFile, HVFile):
    dd["t"] = np.array([0.0])
    dd["W"] = np.loadtxt(os.path.join(path, WFile), skiprows=1)
    dd["HU"] = np.loadtxt(os.path.join(path, HUFile), skiprows=1)
    dd["HV"] = np.loadtxt(os.path.join(path, HVFile), skiprows=1)
    return

################################################################################
# Loads the bathymetry and water level from answer file
################################################################################
def load_answer_data(dd, path):
    ############################
    # TRY TO LOAD THE SIMULATION
    ############################
    W_file = "WaterLevels.txt"
    HU_file = "DischargesX.txt"
    HV_file = "DischargesY.txt"
    # Get the times
    W_path = os.path.join(path, W_file)
    HU_path = os.path.join(path, HU_file)
    HV_path = os.path.join(path, HV_file)
    if Common.check_files(path, [W_file], quiet=True):
        # Read the time
        with open(W_path, "r") as fh:
            # Read first line
            fh.readline()
            # Read second line
            line = fh.readline()
            dd["t"] = np.array(np.mat(line)).flatten()
        # Get the Water Levels
        dd["W"] = np.loadtxt(W_path, skiprows=3)
        # Get the Discharges (optional)
        if Default.SHOW_DISCHARGES and Common.check_files(path, [HU_file, HV_file], quiet=True):
            dd["HU"] = np.loadtxt(HU_path, skiprows=3)
            dd["HV"] = np.loadtxt(HV_path, skiprows=3)
    ########################
    # LOAD THE ARRIVAL TIMES
    ########################
    arrival_times_file = "ArrivalTimes.txt"
    arrival_times_path = os.path.join(path, arrival_times_file)
    if Common.check_files(path, [arrival_times_file], quiet=True):
        dd["arrival_times"] = np.loadtxt(arrival_times_path, skiprows=1)
    ########################
    # LOAD THE WATER LEVELS
    ########################
    # Get the time
    water_levels_file = "MaxWaterHeights.txt"
    water_levels_path = os.path.join(path, water_levels_file)
    # Get the floading
    if Common.check_files(path, [water_levels_file], quiet=True):
        F = np.loadtxt(water_levels_path, skiprows=1)
        dd["initial_dry_cells"] = F[0,:].astype(bool)
        dd["W_max"] = F[1,:]
    # Return, dict already has info saved
    return dd

################################################################################
# Loads a the time series configuration file
################################################################################
def load_query_points_positions(dd, path, TS_file, SP_file,
                                Meter2Degrees=None,
                                show_in_degrees=False):
    """
    Loads the provided configurations for the time series
    """
    dd["time_series"] = load_time_series_config(path, TS_file, show_in_degrees)
    dd["spatial_profile"] = load_spatial_profile_config(path, SP_file, show_in_degrees)
    return

################################################################################
# Loads a the time series configuration file
################################################################################
def load_time_series_config(path, TimeSeriesConfig, load_in_degrees):
    """
    """
    TS_path = os.path.join(path, TimeSeriesConfig)
    if Common.empty_file(TS_path):
        return {"x":[], "y":[], "cells":[]}
    else:
        TS_data = np.loadtxt(TS_path, skiprows=1)
        if load_in_degrees:
            # Convert the Time Series
            if len(TS_data.shape)==1: # Not 2d-array, must convert!
                TS_data = TS_data.reshape(1, len(TS_data))
            old_xy_TS = TS_data[:,:2]
            new_lonlat_TS = LonLatHandler.convert_to_lonlat(old_xy_TS)
            TS_data[:,:2] = new_lonlat_TS

        # We must handle the existence of one Time Series as a special case.
        if len(TS_data.shape)==1:
            # Return as a proper array to allow iteration
            return {"x":np.array([TS_data[0]]),
                    "y":np.array([TS_data[1]]),
                    "cells":np.array([TS_data[2].astype(int)])}
        else:
            return {"x":TS_data[:,0],
                    "y":TS_data[:,1],
                    "cells":TS_data[:,2].astype(int)}


def load_time_series_data(path, filename):
    """
    Loads the data in file assuming underlying format
    """
    if not Common.check_files(path, required_files=filename, quiet=True):
        t, W = [], []
    else:
        # Read the values
        file_path = os.path.join(path, filename)
        data = np.loadtxt(file_path, skiprows=1)
        t = np.array(data[::2])
        W = np.array(data[1::2])
        # FIX FOR BAD SAVING OF TS
        if len(t)>0:
            print("QUICK FIX HERE, DATA HAS SOME ISSUES")
            t0 = t[0]
            ind_final = np.argmax(t0[t0>0])
            t = t[:, :ind_final]
            W = W[:, :ind_final]
            print("END FIX HERE, DATA HAS SOME ISSUES")
    return t, W

################################################################################
# Loads a the spatial profile configuration file
################################################################################
def load_spatial_profile_config(path, SpatialProfileConfig, load_in_degrees):
    """
    """
    SP_path = os.path.join(path, SpatialProfileConfig)
    if Common.empty_file(SP_path):
        return {"t":[], "x":[], "y":[], "cells":[]}
    else:
        # Get the times
        SP_path = os.path.join(path, SpatialProfileConfig)
        with open(SP_path, "r") as fh:
            # Read first line
            fh.readline()
            # Read second line
            line = fh.readline()
            t = np.array(np.mat(line)).flatten()
        # Get the cells
        SP_data =  np.loadtxt(SP_path, skiprows=3)
        if load_in_degrees:
            # Convert the Spatial Profile
            if len(SP_data.shape)==1: # Not 2d-array, must convert!
                SP_data = SP_data.reshape(1, len(SP_data))
            old_xy_SP = SP_data[:,:2]
            new_lonlat_SP = LonLatHandler.convert_to_lonlat(old_xy_SP)
            SP_data[:,:2] = new_lonlat_SP
        # We must handle the existence of one Spatial Profile Point as a special case.
        if len(SP_data.shape)==1:
            # Return as a proper array to allow iteration
            return {"t":t,
                    "x":np.array([SP_data[0]]),
                    "y":np.array([SP_data[1]]),
                    "cells":np.array([SP_data[2].astype(int)])}
        else:
            return {"t":t,
                    "x":SP_data[:,0],
                    "y":SP_data[:,1],
                    "cells":SP_data[:,2].astype(int)}

def load_spatial_profile_data(path, filename):
    """
    Loads the data in file assuming underlying format
    """
    if not Common.check_files(path, required_files=filename, quiet=True):
        t, W = [], []
    else:
        # Read the values
        data = load_raw_data(path, filename, skip_rows=1)
        t = np.array(data[::2])
        W = np.array(data[1::2])
    return t, W

################################################################################
# Loads a plain txt with matrix like entries
################################################################################
def load_raw_data(path, filename, skip_rows=0):
  """
  Loads the data as lists
  """
  # Get the full path
  file_path = os.path.join(path, filename)
  # Get the values
  data = []
  with open(file_path, "r") as fh:
    i=0
    for line in fh.readlines():
      if i>=skip_rows:
        l = np.array([float(v) for v in line.split(" ")])
        data.append(l)
      i+=1
  # Return the values, no transposition required
  return data
