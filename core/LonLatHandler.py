from scipy import interpolate
import numpy as np
import itertools
import os

import Default

if Default.DEBUG_MODE:
    from IPython import embed

def convert_to_lonlat(XYArray):
    print("Linearization Point?")
    Lon0 = float(raw_input("Longitude : "))
    Lat0 = float(raw_input("Latitude : "))
    LP = np.array([Lon0, Lat0])
    return cartesian_to_geographical(XYArray, LP)

def geographical_to_cartesian(LonLatArray, LinearizationPoint):
    """
    Converts the given array of positions in Lat/Lon,
    to position in meters using the Linearization Point.
    It assumes the Longitudes and Latitudes in degrees.
    OBS: To work properly, LonLatArray must be in a continuous range,
    with no jumps.
    """
    # Parameters
    R = 6378140.0 # [m]
    deg2rad = np.pi/180.
    # Return if there's nothing to be converted
    if not len(LonLatArray.shape)==2:
        return LonLatArray
    # Unpack in degrees
    Lon, Lat = LonLatArray[:,0], LonLatArray[:,1]
    Lon0, Lat0 = LinearizationPoint[0], LinearizationPoint[1]
    # Compute the delta angle in radians
    dLat_rad = (Lat-Lat0)*deg2rad
    dLon_rad = (Lon-Lon0)*deg2rad
    # Convert Latitudes
    Y = R * dLat_rad
    # Convert Longitudes
    r = R * np.cos(Lat * deg2rad)     # Minor radius at Latitude for linearization
    X = r * dLon_rad
    # XYArray
    XYArray = np.array([X,Y]).T
    return XYArray

def cartesian_to_geographical(XYArray, LinearizationPoint):
    """
    Converts the given array of positions in cartesian/meters,
    to position in LonLat using the Linearization Point.
    It assumes the Longitudes and Latitudes in degrees.
    """
    # Parameters
    R = 6378140.0 # [m]
    rad2deg = 180./np.pi
    if XYArray.shape[0]==0:
        return XYArray
    # Unpack
    X, Y = XYArray[:,0], XYArray[:,1]
    Lon0, Lat0 = LinearizationPoint[0], LinearizationPoint[1]
    # Compute the Latitude
    Lat_deg = (Y/R)*rad2deg + Lat0
    # Compute the Longitude
    r = R * np.cos(Lat_deg/rad2deg)
    Lon_deg = (X/r)*rad2deg + Lon0
    # XYArray
    LonLatArray = np.array([Lon_deg,Lat_deg]).T
    return LonLatArray
