import os
import time

import Default
import Common
import GMSH

if Default.DEBUG_MODE:
    from IPython import embed

"""
Quaternions and Tracksballs for gmsh viewpoints
"""
Rotation = {}
Trackball = {}

Rotation["x"] = [270., 0., 180.]
Trackball["x"] = [0.48, 0.48, 0.52, 0.52]
Rotation["y"] = [270., 0., 0.]
Trackball["y"] = [0.61, 0., 0., 0.80]
Rotation["z"] = [0., 0., 0.]
Trackball["z"] = [0., 0., 0., 1.]
Rotation["iso"] = [300., 0., 320.]
Trackball["iso"] = [-0.45, -0.15, -0.27, -0.82]

def system(order, quiet=True):
    """ Prints the order, executes and returns the status """
    if not quiet:
        print order
    status = os.system(order)
    return status

def fileformat():
    """ Defines the image format used in images and movie """
    return "file%02d.ppm"

def get_number_of_views(data_path):
    WL = os.path.join(data_path, "ANSWER", "WaterLevels.txt")
    with open(WL, "r") as fh:
        line = fh.readline()
        nW = int(line.split(" ")[-1])
    return nW

def read_view(view_path):
    if not os.path.exists(view_path):
        return False
    with open(view_path, "r") as fh:
        view = fh.readline()
    return view.strip()

def create_images(data_path, sweet_path, images_path, usage_path, view, scale, data_source,
                  show_in_degrees=False):
    """
    Creates the required images for the movie
    """
    RequiredFiles = ["sweet.msh", "sweet_Bdata.msh", "sweet_Wdata.msh", "sweet_discharge_data.msh"]
    imgs_path = os.path.join(sweet_path, "images")
    Common.mkdir(imgs_path)
    trash = os.path.join(imgs_path, "trash")
    # Check if files have been previously created by GMSH, otherwise create them
    if not Common.check_files(sweet_path, RequiredFiles, quiet=True):
        GMSH.create(data_path, sweet_path, usage_path, scale, data_source,
                    show_in_degrees=show_in_degrees)
    # Create the save movie file
    sim_data = os.path.join(sweet_path, "sweet.msh")
    save_movie = os.path.join(imgs_path, "save_movie.gmsh")
    fh = open(save_movie, "w")
    fh.write("General.RotationX = %f;\n" %Rotation[view][0])
    fh.write("General.RotationY = %f;\n" %Rotation[view][1])
    fh.write("General.RotationZ = %f;\n" %Rotation[view][2])
    fh.write("General.TrackballQuaternion0 = %f;\n" %Trackball[view][0])
    fh.write("General.TrackballQuaternion1 = %f;\n" %Trackball[view][1])
    fh.write("General.TrackballQuaternion2 = %f;\n" %Trackball[view][2])
    fh.write("General.TrackballQuaternion3 = %f;\n" %Trackball[view][3])
    nW = get_number_of_views(data_path)
    myfileformat = fileformat()
    for i in range(nW):
        fh.write("View[1].TimeStep = %d;\n" %i)
        filename = myfileformat %i
        fh.write('Print "%s";\n' %filename)
    fh.write("Exit;\n")
    # Launch gmsh with options to save the pictures
    order = "gmsh -p %s %s & >> %s" %(sim_data, save_movie, trash)
    system(order)
    # Create a dummy file to allow rechecking
    order = "echo '%s' > %s" %(view, os.path.join(imgs_path, "view"))
    system(order)
    return

def create_movie(sweet_path, imgs_path, movie_path, viewpoint):
    """
    Creates the movie from the images, but must wait till gmsh has shut down.
    """
    # Use ffmpeg to create the movie
    # OBS: -y forces overwrite of files, -r d has to do with speed, -q:v d with quality
    myfileformat = fileformat()
    Common.mkdir(movie_path)
    images_abspath = os.path.join(imgs_path, myfileformat)
    # Slow
    movie_abspath = os.path.join(movie_path, "movie_%s_slow.avi" %(viewpoint))
    order = "ffmpeg -y -f image2 -r 1 -i %s -r 25 -q:v 1 %s" %(images_abspath, movie_abspath)
    system(order)
    # Medium
    movie_abspath = os.path.join(movie_path, "movie_%s_medium.avi" %(viewpoint))
    order = "ffmpeg -y -f image2 -r 4 -i %s -r 25 -q:v 1 %s" %(images_abspath, movie_abspath)
    system(order)
    # Fast
    movie_abspath = os.path.join(movie_path, "movie_%s_fast.avi" %(viewpoint))
    order = "ffmpeg -y -f image2 -r 8 -i %s -r 25 -q:v 1 %s" %(images_abspath, movie_abspath)
    system(order)
    return

def create(full_path, sweet_full_path, sweet_local_path, view, scale, source, show_in_degrees):
    imgs_full_path = os.path.join(sweet_full_path, "images")
    movies_full_path = os.path.join(sweet_full_path, "movies")
    view_path = os.path.join(imgs_full_path, "view")
    #
    images_folder_does_not_exists = (not os.path.exists(imgs_full_path))
    views_do_not_match = (read_view(view_path)!=view)
    if images_folder_does_not_exists or views_do_not_match :
        create_images(full_path, sweet_full_path, imgs_full_path, sweet_local_path, view, scale, source,
                      show_in_degrees=show_in_degrees)
        print "\nWait until gmsh has finished and run again to create the movie\n"
    else:
        create_movie(sweet_full_path, imgs_full_path, movies_full_path, view)
