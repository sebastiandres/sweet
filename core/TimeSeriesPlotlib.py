from matplotlib import pyplot as plt
import numpy as np
import os

import FileLoader
import Default
import Common

if Default.DEBUG_MODE:
    from IPython import embed

###############################################################################
# Get color properties
###############################################################################
c_ref = Default.reference_values_color
c_ans = Default.answer_values_color
lw_ref = Default.reference_values_lw
lw_ans = Default.answer_values_lw
PLOT_DW = Default.minimum_allowed_difference_for_plot
#image_format = Default.image_format
use_common_scale = Default.use_common_scale

###############################################################################
# Plot all data contained in Time Series
###############################################################################
def create(data_path, imgs_path, imgs_local_path,
           show_in_degrees=False,
           image_format="png"):
    """
    Plots the Time Series data for the given folder
    """
    output_path = os.path.join(data_path, "Output")
    answer_path = os.path.join(data_path, "ANSWER")
    m2deg_path = os.path.join(data_path, "Mesh", "Optional", "Meter2Degrees.txt")
    conf_dic = FileLoader.load_time_series_config(output_path, "TimeSeriesConfiguration.txt", show_in_degrees)
    x_l, y_l = conf_dic["x"], conf_dic["y"]
    if len(x_l)==0 or len(y_l)==0:
        print "No data to display as Time Series"
        return

    # Load the engine
    dd = {}
    FileLoader.load_engine(dd, answer_path)
    engine = dd["engine"]
    # Load simulation values
    t_ans, W_ans = FileLoader.load_time_series_data(answer_path, "TimeSeriesValues.txt")
    # Load true values, if any
    t_ref, W_ref = FileLoader.load_time_series_data(output_path, "TimeSeriesReferenceValues.txt")

    # Check if anything at all to plot
    if len(t_ans)==0 and len(t_ref)==0:
        print "No data to display as Time Series"
	return

    # Compute min-max for better looking plots
    tmin, tmax = Common.get_minmax(t_ref, t_ans)
    Wmin, Wmax = Common.get_minmax(W_ref, W_ans)
    # Do the plots
    for i in range(len(x_l)):
        print "Generating file %d/%d" %(i+1, len(x_l))
        figname = os.path.join(imgs_path, "TimeSeries%03d.%s"%(i,image_format))
        generate_plot(i, x_l, y_l,
                      t_ref, W_ref,
                      t_ans, W_ans,
                      tmin, tmax,
                      Wmin, Wmax, engine,
                      figname,
                      use_common_scale, show_in_degrees)
    # Help
    print "Files have been generated on folder %s" %imgs_local_path
    print "One possible visualization"
    if image_format=="png":
        print "\t eog %s" %os.path.join(imgs_local_path, "TimeSeries*.png")
    if image_format=="pdf":
        print "\t evince %s" %os.path.join(imgs_local_path, "TimeSeries*.pdf")
        print "\t or"
        print "\t pdftk %s cat output combined.pdf" %os.path.join(imgs_local_path, "TimeSeries*.pdf")
        print "\t evince combined.pdf"
    return

###############################################################################
# Plot specific data of Time Series
###############################################################################
def generate_plot(i, x, y,
                  t_ref, W_ref,
                  t_ans, W_ans,
                  tmin, tmax, Wmin, Wmax, engine, figname,
                  use_common_scale, show_in_degrees):
    plt.figure(figsize=(12,8))
    t_unit, t_scale = "sec", 1.
    if (tmax-tmin)/60. > 10:
        t_unit, t_scale = "min", 60.
    if (tmax-tmin)/3600. > 10:
        t_unit, t_scale = "h", 3600.
    # Plot the data
    if len(t_ans)>0:
        plt.plot(t_ans[i]/t_scale, W_ans[i], c_ans, lw=lw_ans, label="answer (%s)" %engine)
    if len(t_ref)>0:
      plt.plot(t_ref[i]/t_scale, W_ref[i], c_ref, lw=lw_ref, label="reference")
    # Fix the scale
    dt = 0.05*(tmax-tmin)
    plt.xlim([(tmin-dt)/t_scale, (tmax+dt)/t_scale])
    if use_common_scale:
        dW = 0.05*(Wmax-Wmin)
        dW = dW if dW>PLOT_DW else PLOT_DW
        plt.ylim([Wmin-dW, Wmax+dW])
    else:
        ymin,ymax = plt.ylim()
        dy = 0.05*(ymax-ymin)
        plt.ylim(ymin-dy, ymax+dy)
    # Put info on plot
    plt.legend(loc="upper right")
    plt.xlabel("Time [%s]" %t_unit)
    plt.ylabel("Water Level [m]")
    if show_in_degrees:
        plt.title("Times Series for Buoy at latitude %.2f degrees and longitude %.2f degrees"%(y[i], x[i]))
    else:
        plt.title("Times Series for Buoy at x=%.2f and y=%.2f, in meters"%(x[i], y[i]))

    # Save and close
    plt.savefig(figname, bbox_inches='tight', dpi=100)
    plt.close()
