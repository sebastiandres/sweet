import numpy as np
import os

import FileLoader
import Default
import Common

TOL_DRY_CELLS = 1E-6
HIDE_DRY_CELLS = True
HIDE_GHOST_CELLS = True
MAX_PLOTTED_ELEMS = 5000

################################################################################
# Loads and saves the corresponding mesh in gmsh format
################################################################################
def create(data_path, imgs_path, usage_path, scale, data_source,
           frame=None,
           show_in_degrees=False,
           zoom_window=None):

    # The required files
    mesh_files = ["NodeCoords.txt", "ElemNodes.txt", "GhostCells.txt"]
    #m2deg_files = ["Meter2Degrees.txt"]
    ic_files = ["Bathymetry.txt",]
    answer_movie_files = ["WaterLevels.txt", "DischargesX.txt", "DischargesY.txt"]
    answer_stats_files = ["MaxWaterHeights.txt", "ArrivalTimes.txt"]
    question_files = ["WaterLevel.txt","DischargeX.txt", "DischargeY.txt"]
    output_files = ["TimeSeriesConfiguration.txt", "SpatialProfilesConfiguration.txt"]

    # The paths where to look for the files
    mesh_path = os.path.join(data_path, "Mesh")
    m2d_path = os.path.join(mesh_path, "Optional")
    ic_path = os.path.join(data_path, "InitialConditions")
    question_path = os.path.join(data_path, "InitialConditions")
    answer_path = os.path.join(data_path, "ANSWER")
    output_path = os.path.join(data_path, "Output")

    # We will load all the data on the dic data_dic
    data_dic = {}

    # Load Mesh if files are present
    if Common.check_files(mesh_path, mesh_files):
        print "\tLoading Mesh files"
        FileLoader.load_mesh(data_dic, mesh_path, *mesh_files,
                             show_in_degrees=show_in_degrees)
    else:
        print "Mesh data files could not be found."
        return

    # Load Bathymetry if files are present
    if Common.check_files(ic_path, ic_files):
        FileLoader.load_bathymetry(data_dic, ic_path, *ic_files)
    else:
        print "Bathymetry files could not be found."
        return

    # Load Time Series and Spatial Profile if files are present
    if Common.check_files(output_path, output_files):
        print "\tLoading Query Points"
        FileLoader.load_query_points_positions(data_dic, output_path, *output_files,
                                               show_in_degrees=show_in_degrees)
    else:
        print "Time Series or Spatial Profile configuration files could not be found."
        return

    # Check if answer solution is present

    if data_source=="answer":
        print "\tLoading ANSWER files"
        # Load the engine
        FileLoader.load_engine(data_dic, answer_path)
        FileLoader.load_answer_data(data_dic, answer_path)
        # Use the fame option
        if (frame is not None) and data_dic.has_key("W"):
            nt = data_dic["W"].shape[0]
            if (0<=frame<nt):
                print "\tShowing only frame %d." %(frame)
                data_dic["t"] = np.array([data_dic["t"][frame]])
                data_dic["W"] = data_dic["W"][frame]
                if data_dic.has_key("HU") and data_dic.has_key("HV"):
                    data_dic["HU"] = data_dic["HU"][frame]
                    data_dic["HV"] = data_dic["HV"][frame]
            else:
                print "\tOption --frame %d not applied. Must verify 0<= f < %d" %(frame, nt)
        # If zoom, just take a few points
        if zoom_window is not None:
            nan_key_list = ["B", "W", "HU", "HV", "W_max", "arrival_times", "initial_dry_cells"]
            Common.update_dict_with_zoom(data_dic, zoom_window, nan_key_list)
        # Write the file
        generate_files(data_dic, imgs_path, usage_path, scale, "m")
        return

    # Fall back on default plot of initial solution (data given by question)
    if data_source=="question":
        print "\tLoading QUESTION files"
        FileLoader.load_question_data(data_dic, question_path, *question_files)

        # If zoom, just take a few points
        if zoom_window is not None:
            nan_key_list = ["B", "W", "HU", "HV"]
            Common.update_dict_with_zoom(data_dic, zoom_window, nan_key_list)
        # Write the file
        generate_files(data_dic, imgs_path, usage_path, scale, "ic")
        return

    return

################################################################################
# Generates the files for gmsh visualization
################################################################################
def generate_files(data_dic, full_path, local_path, scale, flag):
    """
    .
    """
    # Name of files, required to be specified here for compatibility
    main_file = "sweet.msh"
    mesh_file = "sweet_mesh.msh"
    B_file = "sweet_Bdata.msh"
    W_file = "sweet_Wdata.msh"
    discharge_file = "sweet_discharge_data.msh"
    F_file = "sweet_floading_data.msh"
    H_file = "sweet_heatmap_data.msh"
    t_file = "sweet_arrival_times_data.msh"
    TS_file = "sweet_ts.pos"
    SP_file = "sweet_sp.pos"
    created_files = []
    # Start the dict with options to be saved as main file
    views_options = []
    visual_options_dic = {}
    visual_options_dic["General"] = ['TrackballHyperbolicSheet = 1',
                                     'TrackballQuaternion0 = -0.001',
                                     'TrackballQuaternion1 = -0.700',
                                     'TrackballQuaternion2 = -0.720',
                                     'TrackballQuaternion3 = -0.001']
    # Create the mesh
    mesh_options = generate_mesh(full_path, mesh_file, data_dic)
    created_files.append(mesh_file)
    # Obtain the scale
    scale = Common.get_dynamic_scale(data_dic["NodeCoords"], data_dic["B"], scale)

    # Create the bathymetry file
    generate_bathymetry_file(full_path, B_file, data_dic, scale, views_options)
    created_files.append(B_file)
    # Create the Water Level file
    if data_dic.has_key("W"):
        generate_water_level_file(full_path, W_file, data_dic, scale, views_options, flag)
        created_files.append(W_file)
    # Create the Discharge file
    if Default.SHOW_DISCHARGES and data_dic.has_key("HU") and data_dic.has_key("HV"):
        generate_discharge_file(full_path, discharge_file, data_dic, scale, views_options, flag)
        created_files.append(discharge_file)
    # Create the Time Series file
    generate_time_series_file(full_path, TS_file, data_dic, scale, views_options)
    created_files.append(TS_file)
    # Create the Spatial Profile file
    generate_spatial_profile_file(full_path, SP_file, data_dic, scale, views_options)
    created_files.append(SP_file)

    # Save the main file
    visual_options_dic.update({"Mesh":mesh_options, "View":views_options})
    generate_main_file(main_file, visual_options_dic, created_files, full_path)

    # Suggest how to use
    usage(local_path, main_file)
    return

################################################################################
# Generates the Mesh
################################################################################
def generate_mesh(full_path, filename, data_dic):
    print "\tWriting Mesh file"
    fh = open(os.path.join(full_path, filename), "w")
    write_format(fh)
    # Params
    nNodes = data_dic["NodeCoords"].shape[0]
    nElems = data_dic["ElemNodes"].shape[0]
    # Write the nodes
    fh.write('$Nodes\n')
    fh.write('%d\n'%nNodes)
    for i, (xi, yi) in enumerate(data_dic["NodeCoords"]):
        fh.write("%d %.5f %.5f %.5f\n" %(i+1, xi, yi, 0.0))
    fh.write('$EndNodes\n')
    # Write the elements
    elem_type = 2
    tags = 0
    fh.write('$Elements\n')
    fh.write('%d\n'%nElems)
    for j, (n0, n1, n2) in enumerate(data_dic["ElemNodes"]):
        fh.write("%d %d %d %d %d %d\n" %(j+1, elem_type, tags, n0+1, n1+1, n2+1))
    fh.write('$EndElements\n')
    # Close
    fh.close()
    # Mesh options
    options = ["SurfaceEdges = 0", "VolumeEdges = 0"]
    return options

################################################################################
# Generates the bathymetry file
################################################################################
def generate_bathymetry_file(full_path, B_file, data_dic, scale, views):
    print "\tWriting Bathymetry file"
    fh = open(os.path.join(full_path, B_file), "w")
    write_format(fh)
    # Cells to skip or where to put nan
    ghost_cells = get_ghost_cell_list(data_dic["GhostCells"])
    dry_cells = []
    # Write the values
    writeElementData(fh, data_dic["B"], 0.0, 0, ghost_cells, dry_cells, "Bathymetry [meters]")
    fh.close()
    # Graphical options
    options = ['Axes = 1',
               'ColormapNumber = 19',
               'ColormapInvert = 0',
               'ColormapSwap = 0',
               'ShowElement = %d' %show_elements(data_dic),
               'DrawStrings = 0',
               'ShowScale = 0',
               'VectorType = 5',
               'IntervalsType = 3',
               'NbIso = 20',
               'UseGeneralizedRaise = 1',
               'GeneralizedRaiseX = "0"',
               'GeneralizedRaiseY = "0"',
               'GeneralizedRaiseZ = "%f*v%d"'%(scale,0)
               ]
    views.append(options)
    return

################################################################################
# Generates the Water Level(s) file
################################################################################
def generate_water_level_file(full_path, Wfile, data_dic, scale, views, flag):
    print "\tWriting Water Level file"
    # Open File
    fh = open(os.path.join(full_path, Wfile), "w")
    write_format(fh)
    # Cells to skip or where to put nan
    ghost_cells = get_ghost_cell_list(data_dic["GhostCells"])
    # Write the values
    Nt = len(data_dic["t"])
    if Nt==1:
        dry_cells = get_dry_cell_list(data_dic["W"], data_dic["B"])
        if flag=="ic":
            title = "Initial Water Level [m]"
        else:
            title = "Water Levels [m], ANSWER %s (%.3f)" %(data_dic["engine"], data_dic["t"])
        writeElementData(fh, data_dic["W"], data_dic["t"], 0, ghost_cells, dry_cells, title)

    else:
        for i in range(Nt):
            dry_cells = get_dry_cell_list(data_dic["W"][i,:], data_dic["B"])
	    title = "Water Levels [m], ANSWER %s" %(data_dic["engine"])
            writeElementData(fh, data_dic["W"][i,:], data_dic["t"][i], i, ghost_cells, dry_cells, title)
    fh.close()
    # Graphical options
    options = ['ShowElement = %d' %show_elements(data_dic),
               'ColormapNumber = 17',
               'ColormapInvert = 0',
               'ColormapSwap = 1',
               'VectorType = 5',
               'IntervalsType = 3',
               'NbIso = 20',
               'UseGeneralizedRaise = 1',
               'GeneralizedRaiseX = "0"',
               'GeneralizedRaiseY = "0"',
               'GeneralizedRaiseZ = "%f*v%d"'%(scale,0),
               'ShowTime = 1'
               ]
    views.append(options)
    return

################################################################################
# Generates the Discharge(s) file
################################################################################
def generate_discharge_file(full_path, discharge_file, data_dic, scale, views, flag):
    print "\tWriting Discharge file"
    # Open File
    fh = open(os.path.join(full_path, discharge_file), "w")
    write_format(fh)
    # Cells to skip or where to put nan
    ghost_cells = get_ghost_cell_list(data_dic["GhostCells"])
    # Write the values
    Nt = len(data_dic["t"])
    Ncells =  len(data_dic["HU"]) if Nt==1 else data_dic["HU"].shape[1]
    data_i = np.zeros([Ncells,3])
    if Nt==1:
        data_i[:,0] = data_dic["HU"]
        data_i[:,1] = data_dic["HV"]
        dry_cells = get_dry_cell_list(data_dic["W"], data_dic["B"])
        writeElementData(fh, data_i, 0.0, 0, ghost_cells, dry_cells, "Initial Discharge [m2/s]")
    else:
        for i in range(Nt):
            data_i[:,0] = data_dic["HU"][i,:]
            data_i[:,1] = data_dic["HV"][i,:]
            dry_cells = get_dry_cell_list(data_dic["W"][i,:], data_dic["B"])
            writeElementData(fh, data_i, data_dic["t"][i], i, ghost_cells, dry_cells, "Discharge")
    fh.close()
    # Graphical options
    show_map = lambda x: 1 if x=="ic" else 0 # Show only in initial conditions
    options = ['ShowElement = %d' %0, #show_elements(data_dic),
               'ColormapNumber = 17',
               'ColormapInvert = 0',
               'ColormapSwap = 1',
               'Visible = 0',
               'ArrowSizeMax = 100',
               'ArrowSizeMin = 1',
               'VectorType = 4',
               'IntervalsType = 3',
               'NbIso = 20',
               'DisplacementFactor = %.2f'%scale]
    views.append(options)
    return

################################################################################
# Write Time Series pos File
################################################################################
def generate_time_series_file(full_path, filename, data_dic, scale, views):
    print "\tWriting Time Series file"
    fh = open(os.path.join(full_path, filename), "w")
    # Check for content
    if len(data_dic["time_series"])==0:
      fh.close()
      return
    # So we know we have something to write
    # Time Series
    fh.write('View "Time Series Points" {\n')
    x = data_dic["time_series"]["x"]
    y = data_dic["time_series"]["y"]
    cells = data_dic["time_series"]["cells"]
    if data_dic.has_key("W"):
        Z_ts = data_dic["W"]
    else:
        Z_ts = 0*data_dic["B"]
    if len(Z_ts.shape)>1:
        Z_ts = Z_ts.max(axis=0)
    z = scale*np.maximum( Z_ts[cells], data_dic["B"][cells])
    for xi, yi, zi in zip(x,y,z):
        fh.write('SP(%.5f,%.5f,%.5f){0.0};\n' %(xi, yi, zi))
    fh.write('};\n')
    fh.close()
    # gmsh display options
    options = ['Visible = 0',
               'PointSize = 10',
               'PointType = 1',
               'ShowScale = 0',
               'ColormapNumber = 6']
    views.append(options)
    return


################################################################################
# Write Spatial Profile pos File
################################################################################
def generate_spatial_profile_file(full_path, filename, data_dic, scale, views):
    print "\tWriting Spatial Profile file"
    fh = open(os.path.join(full_path, filename), "w")
    # Check for content
    if len(data_dic["spatial_profile"])==0:
      fh.close()
      return
    # So we know we have something to write
    # Time Series
    fh.write('View "Spatial Profile Points" {\n')
    x = data_dic["spatial_profile"]["x"]
    y = data_dic["spatial_profile"]["y"]
    if data_dic.has_key("W"):
        Z_sp = data_dic["W"]
    else:
        Z_sp = 0*data_dic["B"]
    if len(Z_sp.shape)>1:
        Z_sp = Z_sp.max(axis=0)
    cells = data_dic["spatial_profile"]["cells"]
    z = scale*np.maximum( Z_sp[cells], data_dic["B"][cells])
    for xi, yi, zi in zip(x,y,z):
        fh.write('SP(%.5f,%.5f,%.5f){0.0};\n' %(xi, yi, zi))
    fh.write('};\n')
    # Spatial Profile
    fh.close()
    # gmsh display options
    options = ['Visible = 0',
               'PointSize = 10',
               'PointType = 0',
               'ShowScale = 0',
               'ColormapNumber = 0']
    views.append(options)
    return


################################################################################
# Write Element Data for file
################################################################################
def writeElementData(fh, raw_data, t_float, t_int, ghost_cells, unwanted_cells, view_name,
                     delete_ghost_cells_bool=HIDE_GHOST_CELLS,
                     delete_unwanted_cells_bool=HIDE_DRY_CELLS):
    """
    Usually, cells_to_erase will be the dry cells, but we might want to erase wet cells as well.
    """
    # Detect shape
    if len(raw_data.shape)==1:
         nElems, nrows = raw_data.shape[0], 1
    else:
         nElems, nrows = raw_data.shape
    # Now work with the data
    data = raw_data.copy() # to avoid overwrite the loaded data and have collateral surprises
    if delete_unwanted_cells_bool:
        data[unwanted_cells] = np.nan
    if delete_ghost_cells_bool:
        data[ghost_cells] = np.nan
    # Write the Element Data
    fh.write('$ElementData\n')
    fh.write('1\n')               # One name
    fh.write('"%s"\n' %view_name) # The name for the view
    fh.write('1\n')               # One time step
    fh.write('%.2f\n' %t_float)   # The time step value
    fh.write('3\n')               # 3 numbers follow
    fh.write('%d\n' %t_int)       # The index of the time step
    fh.write('%d\n'%nrows)               # The number of values that will be provided
    fh.write('%d\n' %nElems)      # The number of element values
    if nrows==1:
        for j, val_z in enumerate(data):
    	    line = "%d %.5f\n" %(j+1, val_z)
	    fh.write(line)#.replace("nan","NaN"))
    elif nrows==2:
        for j, (val_x, val_y) in enumerate(data):
    	    line = "%d %.5f %.5f\n" %(j+1, val_x, val_y)
	    fh.write(line)#.replace("nan","NaN"))
    elif nrows==3:

        for j, (val_x, val_y, val_z) in enumerate(data):
    	    line = "%d %.5f %.5f %.5f\n" %(j+1, val_x, val_y, val_z)
	    fh.write(line)#.replace("nan","NaN"))
    else:
        print "Data not understood"
    fh.write('$EndElementData\n')
    return

################################################################################
# Generates the main file
################################################################################
def generate_main_file(main_file, main_dic, myfiles, full_path):
    # Write file
    fh = open(os.path.join(full_path, main_file), "w")
    for myfile in myfiles:
        fh.write('Merge "%s";\n' %myfile)
    # Print Mesh options
    for opt in main_dic["Mesh"]:
        fh.write('Mesh.%s;\n' %opt)
    for opt in main_dic["General"]:
        fh.write('General.%s;\n' %opt)
    # Print options Mesh Surface
    for i, opt_list in enumerate(main_dic["View"]):
        for opt in opt_list:
            fh.write('View[%d].%s;\n' %(i, opt))
    # Close it
    fh.close()
    return

################################################################################
# Helper
################################################################################
def write_format(fh):
    fh.write('$MeshFormat\n2.2 0 8\n$EndMeshFormat\n')
    return

################################################################################
# Helper
################################################################################
def get_ghost_cell_list(GhostCells):
    if GhostCells.shape[0]==0:
        return []
    else:
        return GhostCells[:,0]

################################################################################
# Helper
################################################################################
def get_dry_cell_list(W, B):
    dry_cells = np.where(W-B<TOL_DRY_CELLS)[0]
    return dry_cells

################################################################################
# Helper
################################################################################
def show_elements(data_dic):
    showElems = 1 if (data_dic["ElemNodes"].shape[0]<MAX_PLOTTED_ELEMS) else 0
    return showElems

################################################################################
# Print usage help
################################################################################
def usage(path, main_file):
    print 'Visualize with gmsh:'
    print '\tgmsh "%s" &' %(os.path.join(path, main_file))
    return
