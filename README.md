# SWEET #

**Shallow Water Equation Eye Treat** (SWEET) is a python implementation that allows the visualization of inputs and outputs of Shallow Water Equation softwares [QUESTION](https://bitbucket.org/sebastiandres/question/) (SWE Domain Construction) and ANSWER (SWE Numerical Solver).

## Overview ##
Most options just convert the internal formats (SBF and QAS) into files than can be read with [gmsh]([http://geuz.org/gmsh/), a great 3d visualization engine (among many other things). Some specific options generate the 2d plots using mathplotlib and silently save them into the disk. In order to generate movies, the avconverter software is required.

## Requirements ##
* python 2.7
* Matplotlib
* numpy
* gmsh

 
## Help ##

As usual, to get help just type:

    ::bash
    python sweet.py --help

## Further Help ##

For further help, please consult the [wiki](https://bitbucket.org/sebastiandres/sweet/wiki/Home) of the project.