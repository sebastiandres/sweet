# -*- mode: python -*-

block_cipher = None


a = Analysis(['sweet.py'],
             pathex=['/Users/fdduran/src/sweet'],
             hiddenimports=['scipy.integrate'],
             hookspath=None,
             runtime_hooks=None,
             excludes=None)
pyz = PYZ(a.pure)
exe = EXE(pyz,
          a.scripts,
          a.binaries,
          a.zipfiles,
          a.datas,
          name='sweet',
          debug=False,
          strip=None,
          upx=True,
          console=True )
